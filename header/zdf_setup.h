/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Declarations of platform setup routines.
*/

#ifdef _MSC_VER // Use Microsoft specific pre-processor directives for added
                // performance during building
#pragma once
#endif // _MSC_VER

// Header guard
#ifndef ZDF_HEADER_SETUP_H
#define ZDF_HEADER_SETUP_H

extern "C++" namespace zdf_setup {

  namespace python {

  void initialize();

  void analysis();

  } // namespace python

  int sliders(int slider_number);

  namespace initrun {

  void plotting();

  void logging();

  void data();

  void account();

  namespace mode {

  void test();

  void train();

  void trade();

  } // namespace mode

  } // namespace initrun

  namespace exitrun {

  void cycle_information();

  void information();

  } // namespace exitrun

  void first_init();

  void init();

  void exit();

} // namespace zdf_setup

#endif // ZDF_HEADER_SETUP_H
