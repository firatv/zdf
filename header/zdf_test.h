/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Declarations of testing routines.
*/

#ifdef _MSC_VER // Use Microsoft specific pre-processor directives for added
                // performance during building
#pragma once
#endif // _MSC_VER

// Header guard
#ifndef ZDF_HEADER_TEST_H
#define ZDF_HEADER_TEST_H

extern "C++" namespace zdf_test {

  namespace zorro {

  void python_bridge_example();

  namespace workshop {

  void trade_trend();

  double custom_lowpass_filter(double *In, int Period);

  void trade_trend_with_4a_lowpass();

  void trade_counter_trend();

  void double_portfolio();

  void machine_learning();

  } // namespace workshop

  } // namespace zorro

  namespace analysis {

  namespace strategy {

  void c();

  void py();

  } // namespace strategy

  } // namespace analysis

} // namespace zdf_test

#endif // ZDF_HEADER_TEST_H
