/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Declarations of debug routines.
*/

#ifdef _MSC_VER // Use Microsoft specific pre-processor directives for added
                // performance during building
#pragma once
#endif // _MSC_VER

// THIRDPARTY
#include <zorro.h>

// Header guard
#ifndef ZDF_HEADER_DEBUG_H
#define ZDF_HEADER_DEBUG_H

extern "C++" namespace zdf_debug {

  extern int log_target;

  extern bool log_to_file;

  extern int verbose_level;

  extern bool diagnose;

  extern bool log_calls;

  void log(const char *log_message);

  void counter();

  void counter(const char *counter_string);

  void counter(double counter_double);

  void counter(const char *counter_name, double counter_double);

  void array(const char *array_name, double *array, size_t array_size);

  void array(const char *array_name, char *array, const char *delimiters);

} // namespace debug

#endif // ZDF_HEADER_DEBUG_H
