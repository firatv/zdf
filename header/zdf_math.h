/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Declarations of mathematical routines.
*/

#ifdef _MSC_VER // Use Microsoft specific pre-processor directives for added
                // performance during building
#pragma once
#endif // _MSC_VER

// Header guard
#ifndef ZDF_HEADER_MATH_H
#define ZDF_HEADER_MATH_H

extern "C++" namespace zdf_math {

  double calculate_pi();

  double sigmoid(double sigmoid_scalar);

  double safeSigmoid(double safe_sigmoid_scalar);

} // namespace zdf_math

#endif // ZDF_HEADER_MATH_H
