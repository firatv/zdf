/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Declarations of Zorro's Python bridge routines.
*/

#ifdef _MSC_VER // Use Microsoft specific pre-processor directives for added
                // performance during building
#pragma once
#endif // _MSC_VER

// FIRSTPARTY
#include <zdf_global.h>

// Header guard
#ifndef ZDF_HEADER_PYTHON_H
#define ZDF_HEADER_PYTHON_H

extern "C++" namespace zdf_python {

  extern int mode;

  extern bool initialized;

  namespace analysis {

  extern int frequency;
  extern int period;

  extern int bar_counter;

  extern const char *dll_name;

  extern const char *asset_list_name;

  extern const char *data_path;

  extern double *asset_prices;

  extern const int results_size;
  extern double results[];

  extern bool initialized;

  namespace set {

  extern bool state;

  void parameters();

  void data();

  void objects();

  bool all();

  } // namespace set

  bool initialize();

  void trades();

  void strategy();

  void save_data();

  } // namespace analysis

} // namespace zdf_python

#endif // ZDF_HEADER_PYTHON_H
