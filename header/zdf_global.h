/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Declarations of global variables and routines.
*/

#ifdef _MSC_VER // Use Microsoft specific pre-processor directives for added
                // performance during building
#pragma once
#endif // _MSC_VER

// THIRDPARTY
#include <zorro.h>

// Header guard
#ifndef ZDF_HEADER_GLOBAL_H
#define ZDF_HEADER_GLOBAL_H

extern "C++" namespace zdf_global {

  namespace trade {

  extern char size_type;
  extern double size;

  } // namespace trade

  namespace period {

  extern int look_back;

  extern int one_hour;
  extern int four_hours;
  extern int eight_hours;
  extern int twelve_hours;
  extern int one_day;
  extern int one_week;
  extern int one_month;
  extern int one_quarter;
  extern int one_year;

  void set_all(int period_minutes);

  } // namespace period

  void state();

} // namespace zdf_global

#endif // ZDF_HEADER_GLOBAL_H
