/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Declarations of input and output routines.
*/

#ifdef _MSC_VER // Use Microsoft specific pre-processor directives for added
                // performance during building
#pragma once
#endif // _MSC_VER

// Header guard
#ifndef ZDF_HEADER_IO_H
#define ZDF_HEADER_IO_H

extern "C++" namespace zdf_io {

  void replace_characters(char *original_string, const char *character_set,
                          const char replacement_character);

  char *clear_asset_name();

  char *asset_file_name(const char *file_path, const char *file_classifier);

  int convert_string_to_array(char *string, double *array, int array_size);

  void write_prices(const char *file_path, int row_count);

  void read_analysis(const char *file_path, double *analysis_array,
                     int array_size);

} // namespace zdf_io

#endif // ZDF_HEADER_IO_H
