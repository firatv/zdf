/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Definitions of debug routines.
*/

// DECLARATIONS
#include <zdf_debug.h>

// THIRDPARTY
#include <zorro.h>

/* Brief:
     Namespace for elements which are specific to debugging.
*/
extern "C++" namespace zdf_debug {

  // Non-Constant default logging target by print calls.
  extern int log_target = TO_ANY;

  // Non-Constant switch for generating and exporting logs and statistics files
  // in the 'Log' and 'Data' folders.
  extern bool log_to_file = false;

  // Non-Constant switch for defining the verbosity of logging in Zorro (0,
  // 1:default, 2, 3, 7).
  extern int verbose_level = 0;

  // Non-Constant switch for creating a separate file with the last 4000 events,
  // and printing the log also to the message window (Verbose += DIAG).
  extern bool diagnose = false;

  // Non-Constant switch for processing log calls.
  extern bool log_calls = false;

  /* Brief :
  *    Logs to default target using a print call when log calls are
  *    processed.
  */
  void log(const char *log_message) {
    if (log_calls) {
      print(log_target, "\n");
      print(log_target, log_message);
    }
  }

  /* Brief :
  *    Print a counter line.
  *  Notes:
  *    - Somewhat similar to the 'watch' function by Zorro platform.
  *  Overloads:
  *    Print a counter line with a string.
  *    Print a counter line with a double.
  *    Print a named counter line with a double.
  */
  void counter() {
    static unsigned int iteration = 1;
    print(TO_WINDOW, "\n  Counter( %d )\n", iteration);
    iteration++;
  }

  void counter(const char *counter_string) {
    static unsigned int iteration = 1;
    print(TO_WINDOW, "\n  Counter( %d ) = %s\n", iteration, counter_string);
    iteration++;
  }

  void counter(double counter_double) {
    static unsigned int iteration = 1;
    print(TO_WINDOW, "\n  Counter( %d ) = %g\n", iteration, counter_double);
    iteration++;
  }

  void counter(const char *counter_name, double counter_double) {
    static unsigned int iteration = 1;
    print(TO_WINDOW, "\n  Counter( %d ) %s = %g\n", iteration, counter_name,
          counter_double);
    iteration++;
  }

  /* Brief :
  *    Print a named double array.
  *  Overloads:
  *    Print a named string array.
  */
  void array(const char *array_name, double *array, size_t array_size) {
    for (size_t index = 0; index < array_size; index++)
      print(TO_WINDOW, "\n  Array( %s )[%d] = %g\n", array_name, (index + 1),
            array[index]);
  }

  void array(const char *array_name, char *array, const char *delimiters) {
    size_t index = 0;
    char *next_token = nullptr;
    // Initialize the string and get the first string token separated by
    // delimiter(s)
    char *string_token = strtok_s(array, delimiters, &next_token);
    while (string_token != NULL) { // Check at every iteration
      index++;
      print(TO_WINDOW, "\n  Array( %s )[%d] = %s \n", array_name, index,
            string_token);
      string_token =
          strtok_s(NULL, delimiters,
                   &next_token); // Pass NULL to continue with the next token
    }
  }

} // namespace debug
