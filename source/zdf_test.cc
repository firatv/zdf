/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  Namespace 'zorro' Copyright (C) 2023 oP Group Germany
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Definitions of testing routines.
*/

// DECLARATIONS
#include <zdf_debug.h>
#include <zdf_global.h>
#include <zdf_python.h>
#include <zdf_setup.h>
#include <zdf_test.h>

// THIRDPARTY
#include <zorro.h>

/* Brief:
*    Namespace for elements which are specific testing.
*/
extern "C++" namespace zdf_test {

  /* Brief:
  *    Namespace for Zorro examples defined in its documentation.
  *  Notes:
  *    Copyright (C) 2023 oP Group Germany
  */
  namespace zorro {

  /* Brief:
  *    Zorro documentation example for testing its Python bridge.
  *  Notes:
  *    - Python interpreter initialization part is omitted due to
  *      initialization in 'main' by 'zdf_setup::main'.
  *    - The definition of 'PySum' is corrected with additional spaces for
  *      the definition indentation in Python.
  *    - Modified to quit after a single bar due to being called from 'run'
  *      instead of 'main' as in the original example.
  */
  void python_bridge_example() {
    if (Bar == 1) {
      var Vec[5] = {0, 1, 2, 3, 4};
      pySet("PyVec", Vec, 5);
      pyX("for i in range(5): PyVec[i] *= 10\n");
      pyVec("PyVec", Vec, 5);

      int i;
      printf("\nReturned: ");
      for (i = 0; i < 5; i++)
        printf("%.0f ", Vec[i]);

      // test a function
      pyX("def PySum(V):\n    Sum = 0.0\n    for X in V:\n        Sum += X\n   "
          " return Sum\n\n");
      pyX("Result = PySum(PyVec)");
      printf("\nSum: %.0f", pyVar("Result"));
    } else {
      quit("End of Zorro Python bridge test.");
    }
  }

  /* Brief:
  *    Namespace for Zorro workshop examples.
  *  Notes:
  *    - Workshop 8 options trading strategy works with the LiteC compiler only
  *      (as far as I can tell). It needs 'contract.c' to be included, which
  *      creates an include chain (involving 'litec.h' and many more) that
  *      breaks the build.
  */
  namespace workshop {

  /* Brief:
  *    Workshop 4 trend trading algorithm that also introduces time series
  *    usage.
  */
  void trade_trend() {
    set(LOGFILE);

    vars Prices = series(price(0));
    vars Trend = series(LowPass(Prices, 500));

    Stop = 4 * ATR(100);

    vars MMI_Raw = series(MMI(Prices, 300));
    vars MMI_Smooth = series(LowPass(MMI_Raw, 300));

    if (falling(MMI_Smooth)) {
      if (valley(Trend))
        enterLong();
      else if (peak(Trend))
        enterShort();
    }
  }

  /* Brief:
  *    Workshop 4a that introduces indicator implementation.
  *  Parameters:
  *    In (double*): Array of data to be processed.
  *    Period (int): The filter period.
  */
  double custom_lowpass_filter(double *In, int Period) {
    var a = 2. / (1 + Period);
    vars Out = series(In[0], 3);
    return Out[0] = (a - 0.25 * a * a) * In[0] + 0.5 * a * a * In[1] -
                    (a - 0.75 * a * a) * In[2] + (2 - 2 * a) * Out[1] -
                    (1 - a) * (1 - a) * Out[2];
  }

  /* Brief:
  *    This is an example that combines the custom 'custom_lowpass_filter'
  *    indicator in 'Workshop 4a' with the trend trading algorithm in
  *    'Workshop 4'.
  */
  void trade_trend_with_4a_lowpass() {
    set(LOGFILE);

    vars Prices = series(price(0));
    vars Trend = series(custom_lowpass_filter(Prices, 500));

    Stop = 4 * ATR(100);

    vars MMI_Raw = series(MMI(Prices, 300));
    vars MMI_Smooth = series(custom_lowpass_filter(MMI_Raw, 300));

    if (falling(MMI_Smooth)) {
      if (valley(Trend))
        enterLong();
      else if (peak(Trend))
        enterShort();
    }
  }

  /*  Brief:
      Workshop 5 counter-trend trading algorithm that also introduces
      optimization and walk forward analysis.
  */
  void trade_counter_trend() {
    BarPeriod = 240;          // 4 hour bars
    LookBack = 500;           // maximum indicator time period
    set(PARAMETERS, LOGFILE); // generate and use optimized parameters
    StartDate = 2006; // Default installation only has a 'EUR/USD' history that
                      // starts from 2006 (original: 2005).
    EndDate = 2022;   // Default installation only has a 'EUR/USD' history that
                      // ends at 2022 (original: not set).
    NumWFOCycles = 10;
    if (ReTrain) {
      UpdateDays = -1;
      SelectWFO = -1;
    }

    // calculate the buy/sell signal with optimized parameters
    vars Prices = series(price(0));
    vars Filtered = series(BandPass(Prices, optimize(30, 20, 40, 0, 0), 0.5));
    vars Signal = series(FisherN(Filtered, 500));
    var Threshold = optimize(1, 0.5, 1.5, 0.1, 0);

    // buy and sell
    Stop = optimize(4, 2, 10, 0, 0) * ATR(100);
    Trail = 4 * ATR(100);

    if (crossUnder(Signal, -Threshold))
      enterLong();
    else if (crossOver(Signal, Threshold))
      enterShort();

    // plot signals and thresholds
    plot("Filtered", Filtered, NEW, BLUE);
    plot("Signal", Signal, NEW, RED);
    plot("Threshold1", Threshold, 0, BLACK);
    plot("Threshold2", -Threshold, 0, BLACK);
    PlotWidth = 600;
    PlotHeight1 = 300;
  }

  /* Brief:
  *    Workshop 6 double portfolio strategy that also introduces money
  *    management.
  */
  void double_portfolio() {
    set(PARAMETERS, FACTORS, LOGFILE);
    BarPeriod = 60;
    LookBack = 2000;
    StartDate = 2005;
    EndDate = 2022; // Zorro comes with a 'EUR/USD' history that ends at 2022
                    // (original: not set).
    NumWFOCycles = 10;
    Capital = 10000;
    if (ReTrain) {
      UpdateDays = -1;
      SelectWFO = -1;
      reset(FACTORS);
    }

    // double portfolio loop
    while (asset(loop("EUR/USD", "USD/JPY"))) {
      while (algo(loop("TRND", "CNTR"))) {
        Margin = 0.5 * OptimalF * Capital;
        if (0 == strcmp(Algo, "TRND")) {
          TimeFrame = 1; // 1 hour time frame
          vars Price = series(price(0));
          vars Trend = series(LowPass(Price, optimize(500, 300, 700, 0, 0)));
          Stop = optimize(4, 2, 10, 0, 0) * ATR(100);
          Trail = 0;
          vars MMI_Raw = series(MMI(Price, 300));
          vars MMI_Smooth = series(LowPass(MMI_Raw, 500));

          if (falling(MMI_Smooth)) {
            if (valley(Trend))
              enterLong();
            else if (peak(Trend))
              enterShort();
          }
        } else if (0 == strcmp(Algo, "CNTR")) {
          TimeFrame = 4; // 4 hour time frame
          vars Price = series(price(0));
          vars Filtered =
              series(BandPass(Price, optimize(30, 25, 35, 0, 0), 0.5));
          vars Signal = series(FisherN(Filtered, 500));
          var Threshold = optimize(1, 0.5, 2, 0.1, 0);

          Stop = optimize(4, 2, 10, 0, 0) * ATR(100);
          Trail = 4 * ATR(100);
          if (crossUnder(Signal, -Threshold))
            enterLong();
          else if (crossOver(Signal, Threshold))
            enterShort();
        }
      }
    }
  }

  /* Brief:
  *    Workshop 7 strategy that introduces machine learning.
  */
  void machine_learning() {
    StartDate = 2010;
    EndDate = 2018;
    BarPeriod = 1440; // 1 day
    NumWFOCycles = 5; // WFO is mandatory for machine learning functions

    set(RULES, TESTNOW); // generate rules, test after training
    if (Train)
      Hedge = 2; // train with long + short trades
    if (!Train)
      MaxLong = MaxShort = 1; // only 1 open trade
    LifeTime = 1;             // 1 day prediction horizon

    if (adviseLong(PATTERN + 2 + RETURNS, 0, priceH(2), priceL(2), priceC(2),
                   priceH(1), priceL(1), priceC(1), priceH(1), priceL(1),
                   priceC(1), priceH(0), priceL(0), priceC(0)) > 50)
      enterLong();
    if (adviseShort() > 50)
      enterShort();
  }

  } // namespace workshop

  } // namespace zorro

  /* Brief:
  *    Namespace for analysis examples.
  */
  namespace analysis {

  /* Brief:
  *    Namespace for analysis strategies.
  */
  namespace strategy {

  /* Brief:
  *    A simple trend following strategy for simple trades data creation,
  *    similar to Zorro Workshop 6.
  *  Notes:
  *    - Utilizes Zorro functions.
  */
  void c() {
    int period = zdf_global::period::one_day;
    int target_period = zdf_global::period::one_week;
    vars prices = series(price(0));
    vars prices_regression = series(LinearReg(prices, period));
    vars trend = series(Butterworth(prices, period));
    vars mmi_raw = series(MMI(prices, period));
    vars mmi_smooth = series(LowPass(mmi_raw, period));
    if (valley(trend) && falling(mmi_smooth)) {
      Algo = "UPTREND";
      Trail = abs(
          ATR(period) / sqrt(Moment(prices_regression, target_period, 2))
      );
      TakeProfit = abs(MaxVal(trend, target_period) + Trail);
      Stop = abs(MinVal(trend, target_period) - Trail);
      enterLong(1);
    } else if (peak(trend) && falling(mmi_smooth)) {
      Algo = "DOWNTREND";
      Trail = abs(
          ATR(period) / sqrt(Moment(prices_regression, target_period, 2))
      );
      TakeProfit = abs(MinVal(trend, target_period) - Trail);
      Stop = abs(MaxVal(trend, target_period) + Trail);
      enterShort(1);
    }
  }

  /* Brief :
  *    A basic, Python analysis based strategy for a comprehensive trades
  *    data output.
  *  Notes:
  *    - Utilizes Python for all analysis.
  */
  void py() {
    // Loop through all combinations of assets and algorithms
    // 'loop': Initializes Assets and Algorithms up to 40 elements.
    while (asset(loop("EUR/USD", "GBP/USD", "AUD/USD", "USD/JPY", "USD/CAD",
                      "USD/CHF"))) {
      while (algo(loop("TREND", "SCALE"))) {
        // Current algorithm
        static int result_index = 0;
        if (0 == strcmp(Algo, "TREND")) {
          result_index = 0;
          zdf_python::analysis::frequency = zdf_global::period::four_hours;
          zdf_python::analysis::period = zdf_global::period::one_month;
        } else if (0 == strcmp(Algo, "SCALE")) {
          result_index = 1;
          zdf_python::analysis::frequency = zdf_global::period::one_day;
          zdf_python::analysis::period = zdf_global::period::one_week;
        }
        // Analysis
        zdf_python::analysis::strategy();
        static double result = zdf_python::analysis::results[result_index];
        static double result_threshold = 0.5;
        static int lot_size =
            static_cast<int>(zdf_python::analysis::results[result_index + 10]);
        Stop = TakeProfit = zdf_python::analysis::results[result_index + 20];
        // Process
        if (result > result_threshold)
          enterLong(lot_size);
        else if (result < -result_threshold)
          enterShort(lot_size);
      }
    }
  }

  } // namespace strategy

  } // namespace analysis

} // namespace zdf_test
