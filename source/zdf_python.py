#! /usr/bin/env python3
# -*- coding: latin_1 -*-
"""
Copyright 2023-2024 Firat Varol <firatv@protonmail.com>

SPDX-License-Identifier: LGPL-3.0-or-later

Brief :
  Main module for the Zorro Python bridge.

Quickstart :
  1. Execute the module to self-test in an activated environment.

Example :
  ipython -i ./source/zdf_python.py -- -t

Notes :
  - Setup :
    - 'latin_1' (iso-8859-1 - Western Europe) encoding is enforced for compatibility
    in a Windows OS environment. Otherwise interpreter/modules exit with a unicode
    error.
    - Target architecture is 64 bits.
    - Minimum Python version is '3.10'.
    - Python should be added to the PATH.
    - Conda environments are not activated before utilization and fail at imports.
    - PYTHONOPTIMIZE=2 can be set for maximum startup speed.
  - Usage :
    - This file should either be copied to the 'Strategy' folder, or the 'Strategy'
    folder setting in Zorro.ini should point to the parent folder of this file for
    other first party module imports to work. Otherwise an editable install is
    required.
    - Must be called from 'pyStart()'.
  - Design :
    - 'logging.dictConfig' can't override embedded interpreters configuration, thus
    'logging.basicConfig' is utilized with 'force=True'.
"""


# STDLIB
import argparse
import datetime
import itertools
import logging
import math
import os
import pathlib
import signal
import sys
import time
import unittest

# THIRDPARTY
import numpy
import pandas
import scipy
import seaborn
from sklearn import preprocessing, set_config, impute, linear_model


# CONFIGURATION ##############################################################


# VERSION
__version__ = '1.0.1'


# DATE & TIME
TIMEZONE = datetime.timezone.utc
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"  # "0000-00-00 00:00:00"


# LOGGING
program_name = "ZDF"
identifier = "Module"
# Previous/Default, and new values for reconfigurations
LOGGING_PARAMETERS = {
    "level": (logging.root.level, logging.INFO),
    "datefmt": (None, DATETIME_FORMAT),
    "format": (
        None,
        "{levelname}| {asctime}.{msecs:3.0f}| \
{processName}:{process}:{identifier}.{funcName}| {message}",
    ),
    "style": ("%", "{"),
    "force": (True, True),  # Discard previous configurations
}
LOGGING_CONFIGURATION_PREVIOUS = {}
LOGGING_CONFIGURATION = {}
for name, (previous, new) in LOGGING_PARAMETERS.items():
    LOGGING_CONFIGURATION_PREVIOUS[name] = previous
    LOGGING_CONFIGURATION[name] = new
# Reconfigure
logging.basicConfig(**LOGGING_CONFIGURATION)
MODULE_LOGGER = logging.LoggerAdapter(
    logging.getLogger(f"{program_name}_{identifier}"), {"identifier": identifier}
)
MODULE_LOGGER.debug("Configured module logging with :\n%s", LOGGING_CONFIGURATION)


# RANDOMNESS
MODULE_LOGGER.debug("Configuring randomness...")
RANDOM_SEED = 0  # int, array_like[int], None (utilize available system resources)
RANDOM_GENERATOR = numpy.random.default_rng(seed=RANDOM_SEED)
MODULE_LOGGER.debug("Configured randomness with : %s", RANDOM_SEED)


# SCIKIT-LEARN
_SCIKIT_CONFIGURATION = {
    "assume_finite": True,  # Disable 'NaN' checks
}


def _configure_scikit(*args, **kwargs):
    """Configures Scikit-Learn package."""
    global _SCIKIT_CONFIGURATION
    MODULE_LOGGER.debug("Configuring...")
    set_config(**_SCIKIT_CONFIGURATION)
    MODULE_LOGGER.debug("Configured with :\n%s", _SCIKIT_CONFIGURATION)


# SEABORN
_SEABORN_CONFIGURATION = {
    "rc": {
        # "figure.dpi": 600,
        "figure.figsize": (16.0, 9.0),
        "figure.autolayout": True,
    },
    "context": "paper",  # 'paper', 'notebook', 'talk', 'poster'
}


def _configure_seaborn(*args, **kwargs):
    """Configures Seaborn package."""
    global _SEABORN_CONFIGURATION
    MODULE_LOGGER.debug("Configuring...")
    seaborn.set_theme(**_SEABORN_CONFIGURATION)
    MODULE_LOGGER.debug("Configured with :\n%s", _SEABORN_CONFIGURATION)


# ANALYSIS ###################################################################


def analyze_trades_log(*args, **kwargs):
    """Module function for calling "SESSION.analyze_trades_log' without using
    attribute access."""
    global SESSION
    MODULE_LOGGER.debug("Analyzing...")
    SESSION.analyze_trades_log()
    MODULE_LOGGER.debug("Analyzed.")


def analyze_session(*args, **kwargs):
    """Module function for calling "SESSION.analyze' without using attribute
    access."""
    global SESSION
    MODULE_LOGGER.debug("Analyzing...")
    SESSION.analyze()
    MODULE_LOGGER.debug("Analyzed.")


def _truncate_to_int_at_resolution(number, *args, **kwargs) -> int:
    """Truncates given floating point number at sampling resolution depth."""
    MODULE_LOGGER.debug("Truncating %s ...", number)
    truncated = int(math.modf(number / sampling_resolution)[1])
    MODULE_LOGGER.debug("Truncated to %s with %s", truncated, sampling_resolution)
    return truncated


# PARAMETERS
sampling_resolution = 1 * 60  # Minute
frequency = _truncate_to_int_at_resolution(1 * 60**2 * 4)  # 4 Hours
period = _truncate_to_int_at_resolution(1 * 60**2 * 24 * 20)  # 1 Business month
analysis_timestamp = _truncate_to_int_at_resolution(
    datetime.datetime.now(tz=TIMEZONE).timestamp()
)  # 1e-06 resolution


# DATA
data_index = [analysis_timestamp]
asset_prices = []
asset_analysis_results = numpy.full((100,), None)
DATA_PATH = pathlib.Path("./data").resolve()
MODULE_LOGGER.debug("Touching %s", DATA_PATH)
try:
    DATA_PATH.touch(mode=0o664)
except PermissionError as error:
    MODULE_LOGGER.exception(error)
MODULE_LOGGER.debug("Touched : %s", DATA_PATH)


def load_data(*args, **kwargs):
    """Module function for calling "SESSION.load_data' without using attribute
    access."""
    global SESSION
    MODULE_LOGGER.debug("Loading data...")
    SESSION.load_data()
    MODULE_LOGGER.debug("Loaded data.")


def save_data(*args, **kwargs):
    """Module function for calling "SESSION.save_data' without using attribute
    access."""
    global SESSION
    MODULE_LOGGER.debug("Saving data...")
    SESSION.save_data()
    MODULE_LOGGER.debug("Saved data.")


# MIXIN ######################################################################


# INDEPENDENT


class IdentifierMixin:
    """Identifier attributes and routines mixin."""

    def __init__(self, *args, **kwargs):
        global MODULE_LOGGER
        MODULE_LOGGER.debug("Initializing IdentifierMixin...")
        # ATTRIBUTES
        self.class_name = str(self.__class__).rpartition(".")[-1].rstrip("'>")
        self.identifier = kwargs.get("identifier", "IDENTIFIER")
        # Log
        MODULE_LOGGER.debug("Initialized IdentifierMixin")

    def __str__(self):
        if self.identifier == "IDENTIFIER":
            return self.class_name
        else:
            return self.identifier


class OptionMixin:
    """Command line options mixin."""

    def __init__(self, *args, **kwargs):
        global MODULE_LOGGER, RANDOM_SEED, analysis_timestamp
        MODULE_LOGGER.debug("Initializing OptionMixin...")
        # ATTRIBUTES
        if "option" not in kwargs:
            self.option = {
                "debug": False,
                "interactive": False,
                "interval": [
                    analysis_timestamp,
                    analysis_timestamp + (period * 2),  # + lookback ?
                ],  # Test start and end
                "load": False,
                "mode": "test",  # demo, live
                "persistence": "feather",
                "random_seed": RANDOM_SEED,
                "save": True,
                "test": False,
                "verbose": False,
            }
        else:
            self.option = kwargs["option"]
        # Log
        MODULE_LOGGER.debug("Initialized OptionMixin.")


# DEPENDENT


class LoggerMixin:
    """Object logger mixin."""

    def __init__(self, *args, **kwargs):
        global program_name
        # ATTRIBUTES
        self.log = logging.LoggerAdapter(
            logging.getLogger(f"{program_name}_{self.identifier}"),
            {"identifier": self.identifier},
        )
        # Log
        self.log.debug("Initialized LoggerMixin.")


class LoggingMixin:
    """Logging configuration mixin."""

    def __init__(self, *args, **kwargs):
        global LOGGING_CONFIGURATION
        # ATTRIBUTES
        self._logging_configuration = LOGGING_CONFIGURATION
        # CONFIGURATION
        self._configure_logging()
        # Log
        self.log.debug("Initialized LoggingMixin.")

    def _configure_logging(self, *args, **kwargs):
        self.log.debug("Configuring...")
        # Configure with a new dictionary, with updated values
        if self.option["debug"]:
            self._logging_configuration |= {
                "level": logging.DEBUG,
            }
        logging.basicConfig(**self._logging_configuration)
        self.log.debug("Configured with :\n%s", self._logging_configuration)


class ParserMixin:
    """Command line options' routines mixin."""

    def __init__(self, *args, **kwargs):
        global program_name
        self.log.debug("Initializing ParserMixin...")
        # ATTRIBUTES
        self._parser = argparse.ArgumentParser(
            prog=program_name,
            description="""Main module for the Zorro Python bridge.""",
        )
        self._arguments = argparse.Namespace()
        # CONFIGURATION
        self._configure_options()
        self._parse_arguments(*args, **kwargs)
        self._update_options()
        # Log
        self.log.debug("Initialized ParserMixin.")

    def _configure_options(self, *args, **kwargs):
        global TIMEZONE
        self.log.debug("Configuring...")
        for option in self.option.keys():
            match option:
                case "debug":
                    self._parser.add_argument(
                        "-d",
                        "--debug",
                        action="store_true",
                        default=self.option[option],
                        help=f"Change default logging level to 'logging.DEBUG' \
(default: {self.option[option]}).",
                    )
                case "interactive":
                    if {"-i", "ipykernel_launcher"} & {_ for _ in sys.orig_argv}:
                        self.option["interactive"] = True
                case "interval":
                    self._parser.add_argument(
                        "-i",
                        "--interval",
                        metavar="TIMESTAMP",
                        nargs=2,
                        default=self.option[option],
                        help=f"\
POSIX Timestamps of the testing start and end dates \
(default: {self.option[option]}).",
                    )
                case "load":
                    self._parser.add_argument(
                        "-l",
                        "--load",
                        metavar="DATA_PATH",
                        nargs="?",
                        const=True,
                        default=self.option[option],
                        help=f"""Load session components' data. \
(default: {self.option[option]}). \
A path can be passed in to replace sessions default.""",
                    )
                case "mode":
                    self._parser.add_argument(
                        "-m",
                        "--mode",
                        default=self.option[option],
                        choices=["test", "demo", "live"],
                        help=f"Operation mode (default: '{self.option[option]}').",
                    )
                case "persistence":
                    self._parser.add_argument(
                        "-p",
                        "--persistence",
                        default=self.option[option],
                        choices=["csv", "json", "feather"],
                        help=f"Persistence format (default: '{self.option[option]}').",
                    )
                case "random_seed":
                    self._parser.add_argument(
                        "-r",
                        "--random_seed",
                        metavar="SEED",
                        nargs="?",
                        const=None,
                        default=self.option[option],
                        help=f"""Pseudo random number generator seed. \
Can be any of type; None, int, array_like[int] \
(default: {self.option[option]}). In the case of 'None' or '' \
(calling the option without an argument); \
available system entropy is utilized.""",
                    )
                case "save":
                    self._parser.add_argument(
                        "-s",
                        "--save",
                        metavar="DATA_PATH",
                        nargs="?",
                        default=self.option[option],
                        help=f"""Save session components' data. \
(default: {self.option[option]}). \
A path can be passed in to replace sessions default.""",
                    )
                case "test":
                    self._parser.add_argument(
                        "-t",
                        "--test",
                        action="store_true",
                        default=self.option[option],
                        help=f"Run module tests (default: {self.option[option]}).",
                    )
                case "verbose":
                    self._parser.add_argument(
                        "-v",
                        "--verbose",
                        action="store_true",
                        default=self.option[option],
                        help=f"\
Use 'verbose' formatting for log output, as used at the '--debug' option level \
(default: {self.option[option]}).",
                    )
        self.log.debug("Configured with :\n%s", self.option)

    def _parse_arguments(self, *args, **kwargs):
        """Sets defaults in Interactive sessions, and parses command line options."""
        self.log.debug("Parsing command line arguments...")
        # Set defaults when interactive
        if self.option["interactive"]:
            self.log.debug("Interactive mode, setting defaults...")
            self._parser.set_defaults(
                **{
                    key: self._parser.get_default(key)
                    for key in self.option
                    if key != "interactive"  # No default
                }
            )
            self.log.debug("Set defaults.")
        # Parse
        if kwargs.get("parse_known", False):
            self._arguments, unknown_arguments = self._parser.parse_known_args()
            self.log.debug("Unknown arguments :\n%s", unknown_arguments)
        else:
            self._arguments = self._parser.parse_args()
        self.log.debug("Parsed arguments :\n%s", vars(self._arguments))

    def _update_options(self, *args, **kwargs):
        self.log.debug("Updating...")
        self.option |= vars(self._arguments)
        self.option |= {
            "verbose": self._arguments.debug or self._arguments.verbose,
        }
        self.log.debug("Updated options :\n%s", self.option)


class RuntimeMixin:
    """Runtime attributes and routines mixin."""

    def __init__(self, *args, **kwargs):
        self.log.debug("Initializing RuntimeMixin...")
        # ATTRIBUTES
        self.state = 0  # 0: Running, !0: Signal number
        self.timing = {
            "init": time.perf_counter_ns(),
            # Session
            "initialization": 0.0,
            "execution": 0.0,
            "total": 0.0,
            # Event loop
            "enter": 0.0,
            "exit": 0.0,
            # Signals
            "pause": 0.0,
            "continue": 0.0,
            "interrupt": 0.0,
            "hangup": 0.0,
        }
        # Log
        self.log.debug("Initialized RuntimeMixin.")

    def event_loop(self, *args, **kwargs):
        while self.state == 0:
            break

    def run(self, *args, **kwargs):
        self.log.info("Executing...")
        self.timing["enter"] = time.perf_counter_ns()
        # Event loop
        self.log.debug("Entering event loop...")
        self.event_loop()
        self.log.debug("Exited event loop.")
        # Get timings
        self.timing["exit"] = time.perf_counter_ns()
        self.timing["interrupt"] = self.timing["continue"] - self.timing["pause"]
        self.timing["initialization"] = self.timing["enter"] - self.timing["init"]
        self.timing["execution"] = self.timing["exit"] - self.timing["enter"]
        self.timing["total"] = (
            self.timing["initialization"]
            + self.timing["execution"]
            + self.timing["interrupt"]
        )
        for observation in ["interrupt", "initialization", "execution", "total"]:
            self.log.debug(
                "Timing : %s @ %s",
                observation.title(),
                datetime.timedelta(microseconds=self.timing[observation] / 1000),
            )
        # Exit
        self.log.info("Executed.")
        self.exit()

    def exit(self, *args, **kwargs):
        pass


class SignalMixin:
    """System signals' handlers and routines."""

    def __init__(self, *args, **kwargs):
        self.log.debug("Initializing SignalMixin...")
        # ATTRIBUTES
        self._signals = {}
        # CONFIGURATION
        self._initialize_signals()
        # Log
        self.log.debug("Initialized SignalMixin.")

    def _initialize_signals(self, *args, **kwargs):
        """Brief :
             Initializes and validates OS specific signal handlers.
           Notes :
             - 'signal.signal' can only be called from the main thread \
of the main interpreter.
             - The handler is called with two arguments: the signal number \
and the current stack frame (None or a frame object)."""
        self._get_defined_signal_handlers(*args, **kwargs)
        self._validate_signal_handlers(*args, **kwargs)
        self._register_signal_handlers(*args, **kwargs)

    def _get_defined_signal_handlers(self, *args, **kwargs):
        self.log.debug("Getting defined signal handlers...")
        match os.name:
            case "posix":  # Unix, Linux, ..
                self._signals.update(
                    {
                        # Unix
                        signal.strsignal(signal.SIGINT): (
                            signal.SIGINT,
                            self._signal_interrupt_handler,
                        ),
                        signal.strsignal(signal.SIGCONT): (
                            signal.SIGCONT,
                            self._signal_continue_handler,
                        ),
                        signal.strsignal(signal.SIGHUP): (
                            signal.SIGHUP,
                            self._signal_hangup_handler,
                        ),
                        signal.strsignal(signal.SIGCHLD): (
                            signal.SIGCHLD,
                            self._signal_child_handler,
                        ),
                        # Unix or Windows
                        signal.strsignal(signal.SIGABRT): (
                            signal.SIGABRT,
                            self._signal_abort_handler,
                        ),
                        signal.strsignal(signal.SIGTERM): (
                            signal.SIGTERM,
                            self._signal_terminate_handler,
                        ),
                    }
                )
            case "nt":  # Windows
                self._signals.update(
                    {
                        # Unix or Windows
                        signal.strsignal(signal.SIGABRT): (
                            signal.SIGABRT,
                            self._signal_abort_handler,
                        ),
                        signal.strsignal(signal.SIGTERM): (
                            signal.SIGTERM,
                            self._signal_terminate_handler,
                        ),
                    }
                )
            case "java":
                self.log.warning(
                    "No signals are defined for the current execution environment."
                )
        self.log.debug("Got defined signal handlers.")

    def _validate_signal_handlers(self, *args, **kwargs):
        self.log.debug("Validating signal handlers...")
        signals_set = {sig[0] for sig in self._signals.values()}
        valid_signals_set = signal.valid_signals()
        if signals_set < valid_signals_set:  # x 'subset' y
            self.log.debug("All signals are valid.")
        else:
            self.log.error(
                "Mismatch between self._signals : %s\nand signal.valid_signals : %s\n",
                signals_set,
                valid_signals_set,
            )
        self.log.debug("Validated signal handlers.")

    def _register_signal_handlers(self, *args, **kwargs):
        self.log.debug("Registering signal handlers...")
        try:
            if len(self._signals):
                for signal_number, signal_handler in self._signals.values():
                    signal.signal(signal_number, signal_handler)
            else:
                self.log.warning("No signals are defined : %s", self._signals)
        except (ValueError, AttributeError) as undefined_error:
            self.log.exception(undefined_error)
        self.log.debug(
            "Registered signal handlers: %s",
            " ".join(signal[0].name for signal in self._signals.values()),
        )

    def _signal_interrupt_handler(self, signal_number, current_stack):
        match self.state:
            case 0:  # Running
                self.timing["pause"] = time.perf_counter_ns()
                self.log.warning("Pausing with : %s\n", signal.strsignal(signal_number))
                self.state = signal_number
                signal.pause()  # Only in Unix
            case signal_number:  # Interrupted before
                self.timing["continue"] = time.perf_counter_ns()
                self.log.warning(
                    "Continue with : %s\n", signal.strsignal(signal_number)
                )
                self.state = 0
                # 'signal_number' renders remaining case patterns unreachable

    def _signal_continue_handler(self, signal_number, current_stack):
        self.timing["continue"] = time.perf_counter_ns()
        self.log.warning("Continue with : %s\n", signal.strsignal(signal_number))
        self.state = 0

    def _signal_hangup_handler(self, signal_number, current_stack):
        self.timing["hangup"] = time.perf_counter_ns()
        self.log.warning(
            "Hangup detected on controlling terminal or \
death of controlling process (%s).\n",
            signal.strsignal(signal_number),
        )
        self.state = signal_number

    def _signal_child_handler(self, signal_number, current_stack):
        self.log.warning(
            "Child process stopped or signal terminated : '%s'. \
Current stack : \n\n%s\n",
            signal.strsignal(signal_number),
            current_stack,
        )
        self.state = 0

    def _signal_abort_handler(self, signal_number, current_stack):
        self.log.warning("Aborting with : %s\n", signal.strsignal(signal_number))
        self.state = signal_number

    def _signal_terminate_handler(self, signal_number, current_stack):
        self.log.warning("Terminating with : %s\n", signal.strsignal(signal_number))
        self.state = signal_number
        self.exit(signal_number)


class DataMixin:
    """Data attributes and routines."""

    def __init__(self, *args, **kwargs):
        global analysis_timestamp, sampling_resolution, period, DATA_PATH
        self.log.debug("Initializing DataMixin...")
        # ATTRIBUTES
        self._analysis_timestamp = analysis_timestamp
        self._component = {}
        self._data = {}
        self.data_group = {}  # 'name': self.data.groupby('name')
        self._data_path = pathlib.Path(kwargs.get("data_path", DATA_PATH)).resolve()
        self.data_file = kwargs.pop("data_file", None)
        # Log
        self.log.debug("Initialized DataMixin.")

    @property
    def data(self) -> pandas.DataFrame:
        longest = 0
        # Convert all arrays back to 1 dimensional
        for name, array in self._data.items():
            self._data[name] = array.ravel()
            if len(self._data[name]) > longest:
                longest = len(self._data[name])
        # Pad all arrays to the same length
        for name, array in self._data.items():
            self._data[name] = numpy.pad(
                array, [0, longest - len(array)], constant_values=None
            )
        return pandas.DataFrame(data=self._data, copy=False)

    @data.setter
    def data(self, new_data_dict: dict):
        """Converts the data into reshaped (-1, 1) Numpy arrays."""
        for name, data in new_data_dict.items():
            if name not in self._data:
                self._data[name] = []
            self._data[name] = numpy.array(data, copy=False).reshape(-1, 1)

    def register(self, *args, **kwargs):
        """Registers components."""
        name = kwargs["name"]
        self.log.debug(
            "Registration of '%s' as %s ...",
            name,
            " ".join(_.upper() for _ in kwargs.keys() if _ != "name"),
        )
        if "component" in kwargs:
            self._component[name] = (
                {} if kwargs["component"] is True else kwargs["component"]
            )
            setattr(self, name, self._component[name])
        self.log.debug("Registration of '%s' is complete.", name)

    def save_data(self, *args, **kwargs):
        # Check data file path
        if not self.data_file:
            self.data_file = (
                self._data_path
                / f"""\
{self.class_name}_{self.identifier.replace('/', '')}.{self.option['persistence']}"""
            )
        # Process
        if self.option["save"]:
            self.log.info("Saving data of %s ...", self)
            # Start processing from the bottom of the component hierarchy
            if len(self._component):
                self.log.debug("Components : %s", self._component)
                for mapping in self._component.values():
                    for instance in mapping.values():
                        instance.save_data()
            # self.data
            if self.data.empty:
                self.log.info("No data file is output: self.data is empty.")
            else:
                if not isinstance(self.option["save"], bool):
                    self.data_file = self.option["save"]
                # Warn
                if not self.data_file.exists():
                    self.log.debug("Saving %s", self.data_file)
                    match self.option["persistence"]:
                        case "csv":
                            self.data.to_csv(self.data_file, **kwargs)
                        case "json":
                            self.data.to_json(self.data_file, **kwargs)
                        case "feather":  # Binary (depends on 'pyarrow' package)
                            kwargs |= {
                                "compression": "lz4",
                                "compression_level": 9,
                            }
                            self.data.to_feather(self.data_file, **kwargs)
                    self.log.debug(
                        "Saved %s with modified parameters :\n%s",
                        self.data_file,
                        kwargs,
                    )
                else:
                    self.log.warning("Data file exists : %s", self.data_file)
            self.log.info("Saved data of %s", self)

    def load_data(self, *args, **kwargs):
        # Check data file path
        if not self.data_file:
            self.data_file = (
                self._data_path
                / f"""\
{self.class_name}_{self.identifier.replace('/', '')}.{self.option['persistence']}"""
            )
        # Process
        if self.option["load"]:
            self.log.info("Loading data of %s ...", self)
            # Start processing from the bottom of the component hierarchy
            if len(self._component):
                self.log.debug("Components : %s", self._component)
                for mapping in self._component.values():
                    for instance in mapping.values():
                        instance.load_data()
            # self.data
            if not self.data.empty:
                self.log.warning("self.data is not empty :\n%s", self.data.info(1))
            else:
                if not isinstance(self.option["load"], bool):
                    self.data_file = self.option["load"]
                # Warn
                if self.data_file.exists():
                    self.log.debug("Loading %s", self.data_file)
                    match self.option["persistence"]:
                        case "csv":
                            self._data = pandas.read_csv(self.data_file, **kwargs)
                        case "json":
                            self._data = pandas.read_json(self.data_file, **kwargs)
                        case "feather":  # Binary (that depends on 'pyarrow' package)
                            self._data = pandas.read_feather(self.data_file, **kwargs)
                    self.log.info("Loaded %s", self.data_file)
                else:
                    self.log.warning("Data file does not exist : %s", self.data_file)
            self.log.info("Loaded data of %s", self)


class AnalysisMixin:
    """Analysis processors and routines."""

    def __init__(self, *args, **kwargs):
        self.log.debug("Initializing AnalysisMixin...")
        # ATTRIBUTES
        self._report = {}
        # PROCESSORS
        self.impute = impute.KNNImputer(n_neighbors=8, weights="distance", copy=False)
        self.regress = linear_model.LinearRegression()
        self.scale = preprocessing.RobustScaler()
        # Log
        self.log.debug("Initialized AnalysisMixin.")

    @property
    def report(self):
        return self._report

    @report.setter
    def report(self, new_report):
        self._report |= new_report

    def analyze(self, *args, **kwargs):
        pass


# COMPONENT ##################################################################


class ComponentBase(
    IdentifierMixin,
    OptionMixin,
    LoggerMixin,
    DataMixin,
    AnalysisMixin,
):
    """Base for session components."""

    def __init__(self, *args, **kwargs):
        IdentifierMixin.__init__(self, *args, **kwargs)
        OptionMixin.__init__(self, *args, **kwargs)
        LoggerMixin.__init__(self, *args, **kwargs)
        DataMixin.__init__(self, *args, **kwargs)
        AnalysisMixin.__init__(self, *args, **kwargs)
        # Log
        self.log.debug("Initialized ComponentBase of %s %s", self, self.class_name)


# SESSION


class SessionBase(
    IdentifierMixin,
    OptionMixin,
    LoggerMixin,
    ParserMixin,
    LoggingMixin,
    RuntimeMixin,
    SignalMixin,
    DataMixin,
):
    """Base for session state object."""

    def __init__(self, *args, **kwargs):
        global _configure_scikit, _configure_seaborn
        IdentifierMixin.__init__(self, *args, **kwargs)
        OptionMixin.__init__(self, *args, **kwargs)
        LoggerMixin.__init__(self, *args, **kwargs)
        ParserMixin.__init__(self, *args, **kwargs)
        LoggingMixin.__init__(self, *args, **kwargs)
        RuntimeMixin.__init__(self, *args, **kwargs)
        SignalMixin.__init__(self, *args, **kwargs)
        DataMixin.__init__(self, *args, **kwargs)
        # ATTRIBUTES
        self.class_name = self.class_name[:7]
        # CONFIGURATION
        _configure_scikit()
        _configure_seaborn()
        self.register(name="strategy", component=True)
        # Log
        self.log.debug(
            "Initialized %s '%s' in '%s'",
            self.class_name,
            self,
            self._data_path,
        )

    def analyze(self, *args, **kwargs):
        global _update_sampling_parameters
        self.log.debug("%s analysis...", self)
        # Analyze
        for instance in self.strategy.values():
            instance.analyze()
        self.log.debug("%s analysis complete.", self)

    def exit(self, *args, **kwargs):
        signal_number = kwargs.get("signal_number", 0)
        self.log.info(
            "Exiting with : %s",
            signal.strsignal(signal_number) if signal_number > 0 else signal_number,
        )
        sys.exit(signal_number)


class Session(SessionBase):
    """Main session state object."""

    def __init__(self, *args, **kwargs):
        SessionBase.__init__(self, *args, **kwargs)

    def analyze_trades_log(self, *args, **kwargs):
        global current_portfolio
        self.log.debug("Getting trades' logs analysis report...")
        current_portfolio.report["Trades"].analyze()
        self.log.debug("Got trades' logs analysis report.")

    def event_loop(self, *args, **kwargs):
        if self.option["test"]:  # Self test after initialization
            self.log.debug("Self testing...")
            self.test = TestModule()
            self.test.test_session()
            self.log.debug("Self tested.")
        else:
            self.log.debug("%s event loop is undefined.", self)


# STRATEGY


class Strategy(ComponentBase):
    """Manages a single strategy specific portfolio(s)."""

    def __init__(self, *args, **kwargs):
        global strategy
        ComponentBase.__init__(
            self, *args, identifier=kwargs.get("strategy", strategy), **kwargs
        )
        # CONFIGURATION
        self.register(name="portfolio", component=True)
        # Log
        self.log.debug("Initialized '%s' %s", self, self.class_name)

    def analyze(self, *args, **kwargs):
        self.log.debug("%s %s analysis...", self.identifier, self.class_name)
        for instance in self.portfolio.values():
            instance.analyze()
        self.log.debug("%s %s analysis.", self.identifier, self.class_name)


# PORTFOLIO


class Portfolio(ComponentBase):
    """Manages a portfolio of assets."""

    def __init__(self, *args, **kwargs):
        global portfolio
        ComponentBase.__init__(
            self, *args, identifier=kwargs.get("portfolio", portfolio), **kwargs
        )
        # ATTRIBUTES
        self.report |= {
            "Trades": ReportTrades(),
        }
        # CONFIGURATION
        self.register(name="asset", component=True)
        self.register(name="algorithm", component=True)
        # Log
        self.log.debug("Initialized '%s' %s", self, self.class_name)

    def analyze(self, *args, **kwargs):
        self.log.debug("%s %s analysis...", self, self.class_name)
        global asset, algorithm
        self.asset[asset].analyze()
        self.algorithm[algorithm].analyze()
        self.log.debug("%s %s analysis.", self, self.class_name)


# ASSET


class Asset(ComponentBase):
    """Manages a single asset."""

    def __init__(self, *args, **kwargs):
        global period, asset, asset_prices
        ComponentBase.__init__(
            self, *args, identifier=kwargs.get("asset", asset), **kwargs
        )
        # Log
        self.log.debug("Initialized %s '%s'", self.class_name, self)

    # PARAMETERS

    @property
    def prices(self):
        global period, asset_prices
        # Get reversed ([-1] in Python == [0] in Zorro)
        self.data = {"prices": asset_prices[::-1]}
        # Get and return imputed
        self.data = {
            "prices_imputed": self.impute.fit_transform(self._data["prices"][-period:]),
        }
        return self._data["prices_imputed"]

    @property
    def prices_regression(self):
        global period
        prices = self.prices[-period:]
        self.data = {
            "prices_regression": self.regress.fit(
                prices,
                prices.ravel(),
            ).predict(prices),
        }
        return self._data["prices_regression"]


# ALGORITHM


class AlgorithmBase(ComponentBase):
    """Base for algorithms."""

    def __init__(self, *args, **kwargs):
        global algorithm
        ComponentBase.__init__(
            self, *args, identifier=kwargs.get("algorithm", algorithm), **kwargs
        )
        # ATTRIBUTES
        self.class_name = self.class_name[:9]
        self.results_index = kwargs["results_index"]  # asset_analysis_results[n]
        # Log
        self.log.debug("Initialized %s '%s'", self.class_name, self)

    def set_results(self, *args, **kwargs):
        global asset_analysis_results
        for arg_index in range(len(args)):
            asset_analysis_results[self.results_index + (arg_index * 10)] = args[
                arg_index
            ]


class AlgorithmTrend(AlgorithmBase):
    """Trend following algorithm."""

    def __init__(self, *args, **kwargs):
        AlgorithmBase.__init__(self, *args, results_index=0, **kwargs)

    def analyze(self, *args, **kwargs):
        global frequency, period, current_asset
        self.log.debug("Analysis...")
        # Get scaled autocorrelation of prices
        prices_regression = current_asset.prices_regression
        autocorrelation = scipy.signal.correlate(
            prices_regression[-period:],
            prices_regression[-(period + frequency) : frequency],
            method="fft",
        )
        current_asset.data = {"autocorrelation": autocorrelation}
        self.log.debug(
            "autocorrelation(%s)[-1] : %s", len(autocorrelation), autocorrelation[-1]
        )
        autocorrelation_scaled = self.scale.fit_transform(autocorrelation)
        current_asset.data = {"autocorrelation_scaled": autocorrelation_scaled}
        self.log.debug(
            "autocorrelation_scaled(%s)[-1] : %s",
            len(autocorrelation_scaled),
            autocorrelation_scaled[-1],
        )
        # Set
        self.set_results(
            autocorrelation_scaled[-period:].mean(),
            1.0,  # Order size
            numpy.ptp(prices_regression),  # Limit
        )
        self.log.debug("Analysis.")


class AlgorithmScale(AlgorithmBase):
    """Scaled prices regression algorithm."""

    def __init__(self, *args, **kwargs):
        AlgorithmBase.__init__(self, *args, results_index=1, **kwargs)

    def analyze(self, *args, **kwargs):
        global period, current_asset
        self.log.debug("Analysis...")
        # Get scaled prices regression
        prices_regression = current_asset.prices_regression
        prices_regression_scaled = self.scale.fit_transform(prices_regression)
        current_asset.data = {"prices_regression_scaled": prices_regression_scaled}
        self.log.debug(
            "prices_regression_scaled(%s)[-1] : %s",
            len(prices_regression_scaled),
            prices_regression_scaled[-1],
        )
        # Set
        self.set_results(
            prices_regression_scaled[-1],
            1.0,  # Order size
            numpy.ptp(prices_regression),  # Limit
        )
        self.log.debug("Analysis.")


# REPORT


class ReportBase(ComponentBase):
    """Base for reports."""

    def __init__(self, *args, **kwargs):
        global DATA_PATH
        ComponentBase.__init__(self, *args, **kwargs)
        # ATTRIBUTES
        self.class_name = self.class_name[:6]
        self._file_name = kwargs.get(
            "file_name", f"{self.identifier}.{self.option['persistence']}"
        )
        self._data_path = kwargs.get("data_path", DATA_PATH)
        self.data_file = pathlib.Path(self._data_path / self._file_name)
        self.plot = {}
        # Log
        self.log.debug("Initialized '%s' %s", self, self.class_name)


class ReportTrades(ReportBase):
    """Session trades log analysis report."""

    def __init__(self, *args, **kwargs):
        global MODULE_LOGGER, DATA_PATH, strategy
        # ATTRIBUTES
        if "data_path" in kwargs:
            data_path = kwargs["data_path"]
        else:
            data_path = pathlib.Path("../").resolve() / "Log"
            if not data_path.exists():
                MODULE_LOGGER.warning("Data path does not exist : %s", data_path)
                data_path = DATA_PATH
                MODULE_LOGGER.warning(
                    "Continuing with the module data path : %s", data_path
                )
        file_name = kwargs.get("file_name", f"{strategy}_trd.csv")
        # Initialize base class with the updated kwargs
        kwargs |= {
            "identifier": "Trades",
            "file_name": file_name,
            "data_path": data_path,
        }
        ReportBase.__init__(self, *args, **kwargs)

    def initialize_and_prepare_data(self, *args, **kwargs):
        self.log.debug("Getting data...")
        file_data = None
        if self.data_file.exists():
            file_data = pandas.read_csv(
                self.data_file,
                parse_dates=["Open", "Close"],
            )
        else:
            self.log.warning("Data file does not exist '%s'.", self.data_file)
            # Find the most recently modified file
            data_files = list(self._data_path.glob(self.data_file.suffix))
            if len(data_files) > 0:
                self.data_file = sorted(data_files, key=lambda x: x.stat()[-2])[-1]
                file_data = pandas.read_csv(
                    self.data_file,
                    parse_dates=["Open", "Close"],
                )
            else:
                self.log.error(
                    "No data file found at '%s'. Skipping data preparations.",
                    self._data_path,
                )
                return False  # Error
        self.log.debug("Preparing data...")
        # Split strategy and algorithm identifiers into new columns
        extended_data = file_data.assign(
            Strategy=file_data.Name.apply(lambda name: str(name).split(":")[0]),
            Algorithm=file_data.Name.apply(lambda name: str(name).split(":")[1]),
        )
        # Get a duration column in day frequency
        extended_data["Duration"] = (extended_data.Close - extended_data.Open).apply(
            lambda x: x.total_seconds() / 3600 / 24
        )
        # Get analysis data (fixed variables come first, measured variables come later)
        filtered_data = extended_data.loc[
            :,
            [
                "Asset",
                "Type",
                "ExitType",
                "Duration",
                "Profit",
                "Strategy",
                "Algorithm",
            ],
        ]
        filtered_data.Profit += extended_data.Roll
        self.data = filtered_data.to_dict(orient="list")
        self.data_group["Asset"] = self.data.loc[
            :, ["Asset", "Profit", "Duration"]
        ].groupby("Asset", sort=False)
        self.log.debug("Finished.")
        return True

    def plot_data(self, *args, **kwargs):
        self.log.info("Plotting data...")
        # Profits' Empirical Cumulative Distribution
        cumulative_profits_key = "Profits' Empirical Cumulative Distributions"
        self.plot[cumulative_profits_key] = seaborn.displot(
            data=self.data,
            kind="ecdf",
            x="Profit",
            col="Asset",
            col_wrap=3,
            hue="Type",
            palette={"Long": "cyan", "Short": "magenta"},
            alpha=0.5,
        )
        self.plot[cumulative_profits_key].refline(x=0.0, y=0.5)
        # Categorical Profit Distribution
        categorical_profit_key = "Categorical Profit Distribution"
        self.plot[categorical_profit_key] = seaborn.catplot(
            data=self.data,
            kind="violin",
            x="Asset",
            y="Profit",
            hue="Type",
            palette={"Long": "cyan", "Short": "magenta"},
            split=True,
            size="Duration",
        )
        self.plot[categorical_profit_key].refline(y=0.0)
        # Configure, show and save
        self.log.warning("Prepared plots : %s", ", ".join(_ for _ in self.plot.keys()))
        for name, plot in self.plot.items():
            plot.figure.suptitle(name)
            plot.figure.tight_layout()
            plot.figure.savefig(
                self._data_path / f"{self.identifier}-{name.replace(' ', '_')}"
            )
            plot.figure.show()
        self.log.info("Plotted and saved.")

    def analyze(self, *args, **kwargs):
        self.log.debug("%s %s analysis...", self.identifier, self.class_name)
        if self.initialize_and_prepare_data():
            self.plot_data()
        self.log.debug("%s %s analysis.", self.identifier, self.class_name)


# TEST #######################################################################


class TesterBase(IdentifierMixin, OptionMixin, LoggerMixin, RuntimeMixin, DataMixin):
    """Base for testers."""

    def __init__(self, *args, **kwargs):
        IdentifierMixin.__init__(
            self, *args, identifier=kwargs.get("identifier", "Tester"), **kwargs
        )
        OptionMixin.__init__(self, *args, **kwargs)
        LoggerMixin.__init__(self, *args, **kwargs)
        RuntimeMixin.__init__(self, *args, **kwargs)
        DataMixin.__init__(self, *args, **kwargs)
        # ATTRIBUTES
        self.distribution = {
            "exponential": RANDOM_GENERATOR.exponential,
            "gaussian": RANDOM_GENERATOR.normal,
            "logistic": RANDOM_GENERATOR.logistic,
            "lognormal": RANDOM_GENERATOR.lognormal,
            "uniform": RANDOM_GENERATOR.uniform,
        }
        # Log
        self.log.debug("Initialized %s", self.class_name)


class TesterSession(TesterBase):
    """Session unit, and integration tests."""

    def __init__(self, *args, **kwargs):
        global RANDOM_GENERATOR, period
        TesterBase.__init__(self, *args, **kwargs)
        # ATTRIBUTES
        self.lookback = period
        self.interval = []
        self.interval_length = 0
        self.iterations = 0
        self.component_lists = {
            "strategy": ["Test"],
            "portfolio": ["Currency"],
            "asset": ["EUR/USD", "GBP/USD", "AUD/USD", "USD/JPY", "USD/CAD", "USD/CHF"],
            "algorithm": ["TREND", "SCALE"],
        }
        #
        self.component_combinations = [
            dict(zip(self.component_lists.keys(), combination))
            for combination in itertools.product(
                *[_ for _ in self.component_lists.values()]
            )
        ]
        # CONFIGURATION
        self.initialize_test_environment(*args, **kwargs)
        # Log
        self.log.debug(
            "Initialized %s with the components %s",
            self.class_name,
            self.component_lists,
        )

    def initialize_test_environment(self, *args, **kwargs):
        global SESSION, RANDOM_SEED, RANDOM_GENERATOR, sampling_resolution
        global frequency, analysis_timestamp
        global _truncate_to_int_at_resolution, initialize_session
        self.log.debug("Initializing test environment...")
        # Initialize
        initialize_session(*args, parse_known=True, **kwargs)
        self.interval = numpy.arange(
            SESSION.option["interval"][0],
            SESSION.option["interval"][-1],
            1,
        )
        self.interval_length = self.interval[-1] - self.interval[0]
        # Divide by two : loop starts from the middle of the interval [-lookback:]
        self.iterations = int(self.interval_length // frequency / 2)
        # Get testing data
        for asset, distribution_name in zip(
            self.component_lists["asset"], itertools.cycle(self.distribution.keys())
        ):
            distribution_start = RANDOM_GENERATOR.uniform(
                0, 3
            ) ** RANDOM_GENERATOR.uniform(0, 8)
            distribution_data = (
                distribution_start
                + self.distribution[distribution_name](
                    size=self.interval_length
                ).cumsum()
            ).tolist()
            self.log.debug(
                "Distribution '%s' data (%s) : %s",
                distribution_name,
                len(distribution_data),
                distribution_data[-1],
            )
            self.data = {asset: distribution_data}
        # Log
        self.log.debug(
            "Initialized test environment with %s",
            {
                "RANDOM_SEED": RANDOM_SEED,
                "analysis_timestamp": analysis_timestamp,
                "sampling_resolution": sampling_resolution,
                "frequency": frequency,
                "period": period,
                "interval_length": self.interval_length,
                "distribution": [_ for _ in self.distribution.keys()],
            },
        )

    def event_loop(self, *args, **kwargs):
        global SESSION, frequency, analysis_timestamp, asset_prices
        global analyze_session, analyze_trades_log, save_data
        self.log.debug("Creating and analyzing objects...")
        for index in range(0, self.iterations):
            iteration = index + 1
            # Get data window
            slice_start = self.lookback + (frequency * index)
            slice_end = slice_start + frequency
            # Analyze all combinations
            self.log.debug(
                "Iteration %s [%s:%s]",
                iteration,
                slice_start,
                slice_end,
            )
            for combination in self.component_combinations:
                self.log.debug(
                    "@%s | Processing combination : { %s }",
                    iteration,
                    " | ".join(combination.values()),
                )
                asset_prices = self.data[combination["asset"]][slice_start:slice_end]
                for name, value in combination.items():
                    globals()[name] = value  # Update name
                    if iteration == 1:  # Initialize object
                        self.log.debug("Initializing '%s'...", name)
                        globals()["initialize_" + name]()
                        globals()["current_" + name].load_data()
                        self.log.debug("Initialized '%s'", name)
                analyze_session()
                self.log.debug(
                    "@%s | Processed combination : { %s }",
                    iteration,
                    " | ".join(combination.values()),
                )
            self.log.debug("Iteration %s finished.", iteration)
        self.log.debug("Created and analyzed objects.")
        analyze_trades_log()
        save_data()


class TestModule(unittest.TestCase):
    """Test case of the module."""

    # GLOBAL ATTRIBUTES
    tester = {}  # Initialize testers when they are utilized

    def dummy(self, *args, **kwargs):
        """Doctest example :
        >>> {'x': True} | {'y': False}
        {'x': True, 'y': False}
        >>>
        """
        self.assertEqual(1 + 1, 2)

    def test_session(self, *args, **kwargs):
        self.tester["session"] = TesterSession()
        self.tester["session"].run()
        # Revert back to change interpreter log level in interactive mode
        logging.basicConfig(**LOGGING_CONFIGURATION_PREVIOUS)


# FACTORY ####################################################################


# OBJECT IDENTIFIERS
strategy = ""
portfolio = ""
asset = ""
algorithm = ""


# INTERACTIVE SESSION OBJECTS
SESSION = None
current_strategy = None
current_portfolio = None
current_asset = None
current_algorithm = None


# INITIALIZERS
def initialize_session(*args, **kwargs):
    global program_name, SESSION, MODULE_LOGGER
    if SESSION is None:
        if "identifier" not in kwargs:
            kwargs |= {"identifier": program_name}
        SESSION = Session(*args, **kwargs)
        MODULE_LOGGER.info("%s session initialized.", program_name)
    return SESSION


def initialize_strategy(*args, **kwargs):
    global SESSION, strategy, current_strategy
    if "strategy" in kwargs:
        strategy = kwargs["strategy"]
    if strategy not in SESSION.strategy:
        kwargs |= {
            "option": SESSION.option,
        }
        current_strategy = Strategy(*args, **kwargs)
    SESSION.strategy[strategy] = current_strategy
    return current_strategy


def initialize_portfolio(*args, **kwargs):
    global SESSION, strategy, portfolio, current_portfolio
    if "portfolio" in kwargs:
        portfolio = kwargs["portfolio"]
    if portfolio not in SESSION.strategy[strategy].portfolio:
        kwargs |= {
            "option": SESSION.option,
        }
        current_portfolio = Portfolio(*args, **kwargs)
    SESSION.strategy[strategy].portfolio[portfolio] = current_portfolio
    return current_portfolio


def initialize_asset(*args, **kwargs):
    global SESSION, strategy, portfolio, asset, current_asset
    if "asset" in kwargs:
        asset = kwargs["asset"]
    if asset not in SESSION.strategy[strategy].portfolio[portfolio].asset:
        kwargs |= {
            "option": SESSION.option,
        }
        current_asset = Asset(*args, **kwargs)
    SESSION.strategy[strategy].portfolio[portfolio].asset[asset] = current_asset
    return current_asset


def initialize_algorithm(*args, **kwargs):
    global SESSION, strategy, portfolio, algorithm, current_algorithm
    if "algorithm" in kwargs:
        algorithm = kwargs["algorithm"]
    if algorithm not in SESSION.strategy[strategy].portfolio[portfolio].algorithm:
        kwargs |= {
            "option": SESSION.option,
        }
        current_algorithm = globals()[f"Algorithm{algorithm.title()}"](*args, **kwargs)
    SESSION.strategy[strategy].portfolio[portfolio].algorithm[
        algorithm
    ] = current_algorithm
    return current_algorithm


# EXECUTION ##################################################################


if __name__ == "__main__":
    # Log
    MODULE_LOGGER.debug("Executing script...")
    # Run
    initialize_session()
    SESSION.run()
    # Log
    MODULE_LOGGER.debug("Executed script.")
