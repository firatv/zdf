/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Definitions of platform setup routines.
*/

// DECLARATIONS
#include <zdf_setup.h>

// THIRDPARTY
#include <zorro.h>

// FIRSTPARTY
#include <zdf_debug.h>
#include <zdf_global.h>
#include <zdf_python.h>

/* Brief:
*    Namespace for elements which are specific to program setup.
*/
extern "C++" namespace zdf_setup {

  /* Brief:
  *    Namespace for elements specific to Zorro Python bridge setup.
  */
  namespace python {

  /* Brief:
  *    Initializes Zorro Python bridge.
  */
  void initialize() {
    /* pyStart:
    *    Starts Python interpreter, that stays alive until the end of
    *    the Zorro session.
    *  Filename (int):
    *    0 = No file, "file.py" = Loads the file from the 'Strategy' folder.
    *  Mode (int):
    *    0 = None (default).
    *    1 = Log Python output and errors to the files PyOut.txt and
    *        PyError.txt in the Log folder.
    *    2 = Don't release Python variables and modules at session end.
    *    3 = 1 + 2
    */
    if (pyStart("zdf_python.py", zdf_python::mode) == 0)
      zdf_debug::log("Python interpreter failed to initialize.");
    else {
      zdf_python::initialized = true;
      zdf_debug::log("Python interpreter initialized.");
    }
  }

  /* Brief:
  *    Initializes Python analysis variables, objects, etc.
  */
  void analysis() {
    if (!zdf_python::analysis::initialized && !Init) {
      zdf_python::analysis::initialize();
      zdf_debug::log("Python analysis initialized.");
    }
  }

  } // namespace python

  /* Brief :
  *    Sets/Resets sliders.
  *  Parameters:
  *    slider_number (int): The slider to be set/reset.
  */
  int sliders(int slider_number) {
    switch (slider_number) {
    case 1:
      return static_cast<int>(
          slider(1, 2013, 2002, 2023, "Start", "Test start date."));
    case 2:
      return static_cast<int>(
          slider(2, 2014, 2002, 2023, "End", "Test end date."));
    case 3:
      return static_cast<int>(slider(
          3, 0, 0, 10, "Detrend",
          "0 : None\n1 : TRADES\n2 : PRICES\n3 : CURVE\n4 : RECIPROCAL\n5 : "
          "INVERT\n6 : SHUFFLE\n7 : SHUFFLE + PEAK + VALLEY\n8 : BOOTSTRAP\n9 "
          ": BOOTSTRAP + PEAK + VALLEY\n10 : RANDOMWALK"));
    default:
      return 0;
    }
  }

  /* Brief:
  *    Namespace for elements which must be called at INITRUN.
  */
  namespace initrun {

  /* Brief:
  *    Setup plotting related variables, flags, etc. Must be called at INITRUN.
  */
  void plotting() {
    /* Set plotting related Zorro flags:
    *  - PL_ALL: Plot all asset specific curves, regardless of the selected
    *    asset (except for price curve and trades).
    *  - PL_FINE: Plot equity and price curves in higher resolution.
    */
    setf(PlotMode, PL_ALL + PL_FINE);
    zdf_debug::log("setf : PlotMode, PL_ALL + PL_FINE");
    // Bar colors
    ColorUp = GREEN;
    ColorDn = RED;
    // Panel colors
    ColorPanel[0] = BLACK; // Text background
    ColorPanel[1] = BLACK; // Number background
    // Equity colors
    ColorEquity = LIGHTBLUE + TRANSP;
    ColorDD = ORANGE + TRANSP;
  }

  /* Brief:
  *    Setup logging related variables, flags, etc. Must be called at INITRUN.
  */
  void logging() {
    /*  Set logging related Zorro flags:
    *   - LOGFILE: Generate and export logs and trade statistics files in the
    *     Log and Data folders, dependent on 'Test', 'Train', or 'Trade' mode.
    *     Affects the test speed. It is always set in [Trade] mode.
    *   - ALERT: Display critical messages, such as suspicious asset
    *     parameters, possibly orphaned trades, broker API errors, or
    *     print(TO_ALERT,..) calls, in a separate alert box. DIAG recording is
    *     stopped at the first critical event.
    *   - DIAG : A _diag.txt file with a list of the last 4000 events.
    *   - LOGMSG: Print the log also to the message window.
    */
    // Set log file
    if (zdf_debug::log_to_file) {
      set(LOGFILE);
      zdf_debug::log("set : LOGFILE");
    }
    // Set verbosity
    if (zdf_debug::diagnose)
      Verbose = zdf_debug::verbose_level + ALERT + DIAG + LOGMSG;
    else
      Verbose = zdf_debug::verbose_level + ALERT;
  }

  /* Brief:
  *    Setup data related variables, flags, etc. Must be called at INITRUN.
  */
  void data() {
    /* Set price data related Zorro flags:
    *  - COMMONSTART: Delay the simulation until price history of all assets
    *    are available.
    */
    set(COMMONSTART);
    zdf_debug::log("set : COMMONSTART");
    // Zorro sets to 'LookBackNeeded' when at default (80).
    if (zdf_global::period::look_back > 80)
      LookBack = zdf_global::period::look_back;
    UnstablePeriod = min(zdf_global::period::one_day, 40); // Default: 40
    // Set simulation date range. A '0' value uses full history.
    StartDate = sliders(1);
    EndDate = sliders(2);
    // Price data manipulation
    int detrend_mode = 0;
    detrend_mode = sliders(3);
    switch (detrend_mode) {
    case 1: // Removes trend bias from a WFO test while keeping the properties
            // of the price curve.
      Detrend = TRADES;
      break;
    case 2: // Any WFO cycle starts and ends at the same price, and detrends
            // indicators and signals based on series generated with price()
            // calls.
      Detrend = PRICES;
      break;
    case 3: // The curve is tilted so that the end and the start of the
            // historical data are at the same level.
      Detrend = CURVE;
      break;
    case 4: // Replace historical prices with their reciprocal values (e.g:
            // EUR/USD -> USD/EUR).
      Detrend = RECIPROCAL;
      break;
    case 5: // Reverses all trends in the curve, and can be used for a system
            // that is symmetric in long and short positions.
      Detrend = INVERT;
      break;
    case 6: // Randomize WITHOUT replacement. Keep overall trend from start to
            // end, but remove any short-term trends and correlations between
            // the prices.
      Detrend = SHUFFLE;
      break;
    case 7: // Generate a curve that does not exceed the highest peak and the
            // lowest valley.
      Detrend = SHUFFLE + PEAK + VALLEY;
      break;
    case 8: // Randomize WITH replacement. Keep overall trend from start to end,
            // but remove any short-term trends and correlations between the
            // prices.
      Detrend = BOOTSTRAP;
      break;
    case 9: // Generate a curve that does not exceed the highest peak and the
            // lowest valley.
      Detrend = BOOTSTRAP + PEAK + VALLEY;
      break;
    case 10: // Generate a random walk price curve by moving the price in random
             // steps that depend on original volatility.
      Detrend = RANDOMWALK;
      break;
    default: // No detrending
      Detrend = 0;
      break;
    }
  }

  /* Brief:
  *    Setup account related variables, flags, etc. Must be called at INITRUN.
  */
  void account() {
    /* Set account related Zorro flags:
    *  - MAECAPITAL: Calculate the required capital, annual return, and the
    *    'underwater profile' on the chart from maximum adverse excursion.
    *  - MARGINLIMIT: Don't enter a trade when even the minimum amount of 1 Lot
    *    exceeds twice the used Margin value, or when the trade margin plus the
    *    trade risk exceeds the account balance (ignored in TRAINMODE).
    *  - RISKLIMIT: Don't enter a trade when even with the minimum amount of 1
    *    Lot, the trade risk is still higher than twice the than the allowed Risk.
    *    Also don't enter a new trade in TRADEMODE when the total risk of all open
    *    trades exceeds the available margin left in the account.
    *  - ACCUMULATE: Accumulate the Margin of trades skipped by MARGINLIMIT or
    *    RISKLIMIT, until the accumulated margin is high enough to overcome the
    *    limits.
    */
    // set(MAECAPITAL + MARGINLIMIT + RISKLIMIT + ACCUMULATE);
    // zdf_debug::log("set : MAECAPITAL + MARGINLIMIT + RISKLIMIT + ACCUMULATE");
    // Set a limit of maximum number of Lots with the current asset for safety
    // (default: 1000000000/LotAmount)
    LotLimit = 10000000 / LotAmount; // LotAmount default: 1000 = micro, 10000 =
                                     // mini, 100000 = standard
    // Set account specifications
    switch (zdf_global::trade::size_type) {
    case 'L':
      Lots = static_cast<int>(zdf_global::trade::size); // Default: 1
      break;
    case 'A':
      Amount = zdf_global::trade::size; // Default: 0 = trade size given by Lots
      break;
    case 'M':
      Margin = zdf_global::trade::size; // Default: 0.00000001 = for always
                                        // opening at least 1 lot
      break;
    case 'R':
      Risk = zdf_global::trade::size; // Default: 0 = No limit
      break;
    default: // Lots
      break;
    }
  }

  /* Brief:
  *    Namespace for elements which are specific to execution modes.
  */
  namespace mode {

  /* Brief:
  *    Setup TESTMODE related variables, flags, etc. Must be called at INITRUN.
  */
  void test() {
    /* Set TESTMODE related Zorro flags:
    *  - OPENEND: Do not close the remaining open trades at the end of a
    *    simulation cycle; leave them open and ignore their results.
    *  - RECALCULATE: Run a full LookBack period at the begin of every WFO
    *    cycle in TESTMODE. Must be set in INITRUN.
    */
    set(OPENEND + RECALCULATE);
    zdf_debug::log("set : OPENEND + RECALCULATE");
    // Static window title (after 'Script' and 'AssetList' is set)
    print(TO_TITLE, "Test: %s: %s", Script, AssetList);
  }

  /* Brief:
  *    Setup TRAINMODE related variables, flags, etc. Must be called at
  *    INITRUN.
  */
  void train() {
    /* Set TRAINMODE related Zorro flags:
    *  - ALLCYCLES: Do not reset the statistics values inside the STATUS
    *    structs when a new sample cycle is started.
    *  - TESTNOW: Run a test immediately after training.
    */
    set(ALLCYCLES + TESTNOW);
    zdf_debug::log("set : ALLCYCLES + TESTNOW");
    // Static window title (after 'Script' and 'AssetList' is set)
    print(TO_TITLE, "Train: %s: %s", Script, AssetList);
  }

  /* Brief:
  *    Setup TRADEMODE related variables, flags, etc. Must be called at
  *    INITRUN.
  */
  void trade() {
    /* Set TRADEMODE related Zorro flags (with 'setf/resf' to avoid modifying
    *  all flags including defaults):
    *  - SV_ALGOVARS2: Save/Load all 'AlgoVars2'.
    *  - SV_STATS: Save/Load statistics data, to be continued in the next
    *    session.
    */
    setf(TradeMode, SV_ALGOVARS2 + SV_STATS);
    zdf_debug::log("setf : TradeMode, SV_ALGOVARS2 + SV_STATS");
    // Static window title (after 'Script' and 'AssetList' is set)
    print(TO_TITLE, "%s: %s: %s", (is(DEMO) ? "Demo" : "Live"), Script,
          AssetList);
  }

  } // namespace mode

  } // namespace initrun

  /* Brief:
  *    Namespace for elements which must be called at EXITRUN.
  */
  namespace exitrun {

  /* Brief :
  *    Display the sample and/or WFO cycle information in 'WINDOW' section of
  *    the display. Preferably called at EXITRUN.
  */
  void cycle_information() {
    // Display cycle iterations
    if (NumSampleCycles > 1) // Current sample cycle (when oversampling)
      print(TO_ANY, "\nSample cycle : % d", SampleCycle);
    if (NumWFOCycles > 0) // Walk forward analysis cycle
      print(TO_ANY, "\nWFO cycle : %d", WFOCycle);
    // Display balance of the current sample and/or WFO cycle
    print(TO_ANY, "\nCycle balance : %.2f", Balance);
    // Print a cycle/WFO end separator
    zdf_debug::log("_____________________ CYCLE ______________________");
  }

  /* Brief :
  *    Display the sample and/or WFO cycle information in 'WINDOW' section of
  *    the display. Preferably called at EXITRUN.
  */
  void information() {
    if (NumSampleCycles > 1 || NumWFOCycles > 0)
      cycle_information();
    // Print a simulation end separator
    zdf_debug::log("____________________ PROCESS _____________________");
  }

  } // namespace exitrun

  /* Brief:
  *    Setup of the first initial run of the whole simulation.
  */
  void first_init() {
    // Setup output
    initrun::logging();
    initrun::plotting();
    // Periods ('slider' must be called in INITRUN)
    zdf_global::period::set_all(static_cast<int>(slider(0)));
    // Initialize Python bridge
    python::initialize();
    // Inform the end of execution step
    zdf_debug::log("_________________ FIRSTINITRUN __________________");
  }

  /* Brief:
  *    Setup of the initial run of the sample/WFO cycle, BEFORE price data is
  *    loaded and log file is opened.
  */
  void init() {
    // Setup input
    initrun::data();
    initrun::account();
    // Setup execution modes
    if (Test)
      initrun::mode::test();
    // else if (Train) initrun::mode::train();
    else if (Live)
      initrun::mode::trade();
    // Inform the end of execution step
    zdf_debug::log("____________________ INITRUN _____________________");
  }

  /* Brief:
  *    Setup of the last run of the whole simulation, or a quit call (without
  *    a preceding '#').
  */
  void exit() {
    // Process and display analysis
    zdf_python::analysis::trades();
    zdf_python::analysis::strategy();
    zdf_python::analysis::save_data();
    exitrun::information();
    // Reset sliders
    for (int i = 1; i < 4; i++) {
      sliders(i);
    }
    // Inform the end of execution step
    zdf_debug::log("____________________ EXITRUN _____________________");
  }

} // namespace zdf_setup
