/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Definitions of global variables and routines.
*/

// DECLARATIONS
#include <zdf_global.h>
#include <zdf_python.h>

// THIRDPARTY
#include <zorro.h>

/* Brief:
*    Namespace for elements which are global.
*/
extern "C++" namespace zdf_global {

  /* Brief:
  *    Namespace for elements which are specific to trading.
  */
  namespace trade {

  // Non-Constant trade size type ; (L)ots, (A)mount, (M)argin, (R)isk.
  extern char size_type = 'L';

  // Non-Constant trade size.
  extern double size = 0.0;

  } // namespace trade

  /* Brief:
  *    Namespace for elements which are specific to bar periods.
  */
  namespace period {

  // Non-Constant LookBack period
  extern int look_back = 0;

  // Non-Constant Bar periods
  extern int one_hour = 0;
  extern int four_hours = 0;
  extern int eight_hours = 0;
  extern int twelve_hours = 0;
  extern int one_day = 0;
  extern int one_week = 0;
  extern int one_month = 0;
  extern int one_quarter = 0;
  extern int one_year = 0;

  /* Brief:
  *    Sets bar periods to given minutes.
  *  Parameters:
  *    period_minutes (int): Bar period minutes
  */
  void set_all(int period_minutes) {
    switch (period_minutes) {
    case 1: // 1 Minute
      one_hour = 60;
      break;
    case 5: // 5 Minutes
      one_hour = 12;
      break;
    case 15: // 15 Minutes
      one_hour = 4;
      break;
    case 30: // 30 Minutes
      one_hour = 2;
      break;
    case 60: // 60 Minutes
      one_hour = 1;
      break;
    default: // 60 Minute
      print(TO_ANY, "\nPossible bar period values are: 1, 5, 15, 30, 60. "
                    "Defaulting to '60'.\n");
      one_hour = 1;
      break;
    }
    // Set other periods
    four_hours = one_hour * 4;
    eight_hours = four_hours * 2;
    twelve_hours = four_hours * 3;
    one_day = twelve_hours * 2;
    one_week = one_day * 5;
    one_month = one_week * 4;
    one_quarter = one_month * 3;
    one_year = one_quarter * 4;
  }

  } // namespace period

  /* Brief :
  *    Display state using sliders and 'INFO' section of the display.
  */
  void state() {
    // Slider values to be displayed during test/trade
    slider(1, MarginMax, 0.0, 0.0, "Margin",
           "Maximum margin sum of open trades in account currency units.");
    slider(2, RiskMax, 0.0, 0.0, "Risk",
           "Estimated maximum open risk of trades in account currency units.");
    slider(3, DrawDownMax, 0.0, 0.0, "DD", "Maximum drawdown");
    // Print the balance and won/lost trade counts to info/progress bar
    static bool any_open_trades =
        (NumOpenTotal > 0 || NumOpenPhantom > 0 ? true : false);
    if (any_open_trades) {
      if (is(LOOKBACK))
        print(TO_INFO, "Lookback : %d / %d", Bar, LookBack);
      else
        print(TO_INFO, "Simulation : %d", Bar);
    } else {
      print(TO_INFO, "%.0f %d/%d:%d %.1f", Balance, NumWinTotal, NumLossTotal,
            NumOpenTotal, TradeVal);
    }
  }

} // namespace zdf_global
