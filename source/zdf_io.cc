/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Definitions of input and output routines.
*/

// DECLARATIONS
#include <zdf_io.h>

// THIRDPARTY
#include <zorro.h>

// FIRSTPARTY
#include <zdf_debug.h>

/* Brief:
*    Namespace for elements which are specific to input and output.
*/
extern "C++" namespace zdf_io {

  /* Brief:
  *    Replaces a given set of characters in a string with another character.
  *  Parameters:
  *    original_string (char*): The string to be processed.
  *    character_set (const char*): The set of characters to be removed.
  *    replacement_character (const char): Replacement for removed characters.
  */
  void replace_characters(char *original_string, const char *character_set,
                          const char replacement_character) {
    size_t char_index =
        strcspn(original_string,
                character_set); // No return value is reserved for an error
    while (char_index !=
           strlen(original_string)) { // An invalid character found
      original_string[char_index] = replacement_character; // Replace it
      char_index = strcspn(original_string,
                           character_set); // Check again until all are replaced
    }
  }

  /* Brief:
  *    Clears an asset name that is to be used in a file name from invalid
  *    characters.
  *  Notes:
  *    - Uses '_strdup' to duplicate which utilizes 'malloc', because strcpy_s
  *      doesn't work as expected when called from Zorro.
  */
  char *clear_asset_name() {
    const int asset_name_length = 30;
    static char asset_name[asset_name_length];
    char *asset_name_copy = _strdup(Asset);
    if (asset_name_copy == NULL) {
      zdf_debug::log("ERROR : _strdup == NULL.");
    } else {
      replace_characters(asset_name_copy, "\\*/<:>|?\"", '-');
      strcpy_s(asset_name, sizeof(char) * asset_name_length, asset_name_copy);
      free(asset_name_copy);
    }
    return asset_name;
  }

  /* Brief:
  *    Returns an asset specific file name.
  *  Parameters:
  *    file_path (const char*): Output directory for the file.
  *    file_classifier (const char*): The contents of the file (e.g: 'Prices',
  *    'Configuration', etc.)
  */
  char *asset_file_name(const char *file_path, const char *file_classifier) {
    const int file_name_size = 200;
    static char asset_file_name[file_name_size];
    if (sprintf_s(asset_file_name, sizeof(char) * file_name_size, "%s%s%s%s%s",
                  file_path, file_classifier, "_(", clear_asset_name(),
                  ").txt") < 0)
      zdf_debug::log("ERROR : sprintf_s() < 0");
    return asset_file_name;
  }

  /* Brief:
  *    Converts given string to a double array.
  *  Parameters:
  *    string (char*): The string to be converted.
  *    array (double*): The array to hold the converted data.
  *    array_size (int): The size of the array.
  */
  int convert_string_to_array(char *string, double *array, int array_size) {
    // Get the first string token seperated by the delimiter(s)
    const char Delimiters[] = " ,\n";
    char *NextToken = nullptr;
    char *StringToken = strtok_s(string, Delimiters, &NextToken);
    if (StringToken == NULL) {
      zdf_debug::log("ERROR : StringToken[0] !");
      return 1;
    } else {
      // Convert the first string token (and get the stopping character if one
      // is encountered)
      double StringDouble = 0.0;
      char *StopToken = nullptr;
      StringDouble = strtod(StringToken, &StopToken);
      if (StringDouble != NULL || StopToken == nullptr)
        array[0] = StringDouble;
      else {
        zdf_debug::log("ERROR : strtod : StopToken[0]");
        return 1;
      }
      // Get the rest of the string tokens
      for (int i = 1; i < array_size; i++) {
        StringToken = strtok_s(NULL, Delimiters, &NextToken);
        if (StringToken != NULL) { // Check
          StopToken = nullptr;     // Reset before conversion
          StringDouble =
              strtod(StringToken, &StopToken); // Write the first string token
          if (StringDouble != NULL || StopToken == nullptr)
            array[i] = StringDouble;
          else {
            zdf_debug::log("ERROR : strtod : StopToken[i]");
            return 1;
          }
        }
      }
      return 0;
    }
  }

  /* Brief:
  *    Writes an assets prices to a text file.
  *  Parameters:
  *    file_path (const char*): Output directory for the file.
  *    row_count (int): The length of the file.
  *  Notes:
  *    - Uses 'calloc' to initialize the prices array with zeros in case there
  *      are missing prices in the output.
  *    - Uses 'file_write' from Zorro platform because other methods (e.g:
  *      'fwrite', 'std::ofstream', etc.) doesn't work as expected when called
  *      from Zorro.
  */
  void write_prices(const char *file_path, int row_count) {
    const int row_length = 30; // 16 significant digits + dot + NULL
    char row[row_length];
    char *file_string = (char *)calloc(
        static_cast<size_t>(row_length * row_count), sizeof(char));
    if (file_string == NULL)
      zdf_debug::log("ERROR : file_string == NULL");
    else {
      // Get prices
      for (int i = 0; i < row_count; i++) {
        sprintf_s(row, row_length, "%.16g\n", price(i, 0)); // Format each row
        strcat_s(file_string, rsize_t(row_length * row_count), row); // Append
      }
      // Write to file
      if (file_write(asset_file_name(file_path, "Prices"), file_string, 0) == 0)
        zdf_debug::log("ERROR : file_write");
      free(file_string);
    }
  }

  /* Brief:
  *    Reads analysis file into a double array.
  *  Parameters:
  *    file_path (const char*): Output directory for the file.
  *    analysis_array (double*): The array for the file data.
  *    array_size (int): The size of the array.
  *  Notes:
  *    - Uses 'calloc' to initialize the prices array with zeros in case there
  *      are missing prices in the output.
  *    - Uses 'file_read' from Zorro platform because other methods (e.g:
  *      'fread', 'std::ifstream', etc.) doesn't work as expected when called
  *      from Zorro.
  */
  void read_analysis(const char *file_path, double *analysis_array,
                     int array_size) {
    const int row_length = 30; // 16 significant digits + dot + NULL
    char *file_string = (char *)calloc(
        static_cast<size_t>(row_length * array_size), sizeof(char));
    if (file_string == NULL)
      zdf_debug::log("ERROR : file_string == NULL");
    else {
      // Read string from file
      if (file_read(asset_file_name(file_path, "Analysis"), file_string,
                    (array_size * row_length * sizeof(char)) != 0)) {
        // Convert to double
        if (convert_string_to_array(file_string, analysis_array, array_size) !=
            0)
          zdf_debug::log("ERROR : convert_string_to_array");
      } else
        zdf_debug::log("ERROR : file_read");
      free(file_string);
    }
  }

} // namespace zdf_io
