/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Definitions of mathematical routines.
*
*  Notes:
*    - Standard C library functions are utilized.
*/

// DECLARATIONS
#include <zdf_math.h>

// STDLIB (C)
#include <cmath>

/* Brief:
*    Namespace for elements which are specific to custom mathematical
*    operations.
*/
extern "C++" namespace zdf_math {

  /* Brief :
  *    Calculates PI.
  *  Notes:
  *    - Uses standard C library function 'atan2'.
  */
  double calculate_pi() { return std::atan2(1.0, 1.0) * 4.0; }

  /* Brief :
  *    Sigmoid normalization function.
  *  Parameters:
  *    sigmoid_scalar (double): The scalar to be normalized.
  *  Notes:
  *    - Uses standard library (std) function 'exp'.
  */
  double sigmoid(double sigmoid_scalar) {
    return (1.0 / (1.0 + std::exp(-sigmoid_scalar)));
  }

  /* Brief :
  *    Custom sigmoid normalization function with an output in range [1,2] that
  *    can be used as a safe multiplier/divisor.
  *  Parameters:
  *    safe_sigmoid_scalar (double): The scalar to be normalized.
  */
  double safeSigmoid(double safe_sigmoid_scalar) {
    return (1.0 + sigmoid(safe_sigmoid_scalar));
  }

} // namespace zdf_math
