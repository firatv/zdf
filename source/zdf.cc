/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Main source file that defines execution routines.
*/

// FIRSTPARTY
#include <zdf_debug.h>
#include <zdf_global.h>
#include <zdf_python.h>
#include <zdf_setup.h>
#include <zdf_test.h>

// THIRDPARTY
#include <zorro.h>

/* Brief:
*    Run function of the strategy that is called per Bar/Tick/PriceQuote.
*  Notes:
*    - Exported with the calling convention '__declspec'.
*/
extern "C" __declspec(dllexport) void run() {
  if (false) { // Zorro examples
    // zdf_test::zorro::python_bridge_example();
    // zdf_test::zorro::workshop::trade_trend_solo();
    // zdf_test::zorro::workshop::trade_trend_with_4a_lowpass();
    // zdf_test::zorro::workshop::trade_counter_trend_solo();
    zdf_test::zorro::workshop::double_portfolio();
    // zdf_test::zorro::workshop::machine_learning();
  } else { // ZDF
    // Setup
    if (is(FIRSTINITRUN)) {
      // Setup system
      zdf_debug::log_to_file =
          true; // Generate and export logs and statistics files in the Log and
                // Data folders (set(LOGFILE)).
      zdf_debug::log_calls = true; // Process 'zdf_debug::log' calls.
      zdf_python::mode = 3;
      zdf_setup::first_init(); // Initialize periods according to slider[0]
      // Setup strategy
      zdf_global::period::look_back = zdf_global::period::one_month * 2;
      Hedge = 0; // 0: None, 1: Across algorithms, 2: Full, 4: Virtual with
                 // partial closing, 5: Virtual with partial closing and
                 // pooling, 6: Script synchronized, virtual with partial
                 // closing and pooling (default: 0)
      Leverage = 100.0; // default: 0
      Capital = 0.0; // Used for determining the effectivity of the reinvestment
                     // algorithm. No Monte Carlo analysis is done if set.
                     // (default: 0 = No initial capital).
    }
    if (Init) { // Cycle initialization
      zdf_setup::init();
    }
    // Process
    else {
      // Must be called after 'Init' of assets and data
      zdf_setup::python::analysis();
      // Test
      // zdf_test::analysis::strategy::c();
      zdf_test::analysis::strategy::py();
      // Display progress
      zdf_global::state();
    }
    // Wrap up
    if (is(EXITRUN)) {
      zdf_setup::exit();
    }
  }
}
