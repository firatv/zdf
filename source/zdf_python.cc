/* Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
*
*  SPDX-License-Identifier: LGPL-3.0-or-later
*
*  Brief:
*    Definitions of Zorro's Python bridge routines.
*/

// DECLARATIONS
#include <zdf_python.h>

// THIRDPARTY
#include <zorro.h>

// FIRSTPARTY
#include <zdf_global.h>

/* Brief:
     Namespace for elements which are specific Python.
*/
extern "C++" namespace zdf_python {

  /* Non-Constant 'pyStart' mode
  *  0 = None (default).
  *  1 = Log Python output and errors to the files PyOut.txt and PyError.txt in
  *      the Log folder.
  *  2 = Don't release Python variables and modules at session end ('numpy'
  *      package does not support multiple runs with embedded Python).
  *  3 = 1 + 2
  */
  extern int mode = 0;

  // Non-Constant Python bridge initialization switch
  extern bool initialized = false;

  /* Brief:
  *    Namespace for analysis examples.
  */
  namespace analysis {

  // Algorithm specific analysis sample sizes (utilized in data shapes)
  extern int frequency = 0;
  extern int period = 0;

  // Non-Constant Bar counter
  extern int bar_counter = zdf_global::period::one_hour;

  // DLL file name
  extern const char *dll_name = "ZDF_Release_64x";

  // Asset list file name
  extern const char *asset_list_name = "AssetsFix";

  // Data file dumps path
  extern const char *data_path = "/Log";

  // Asset data
  extern double *asset_prices = series(price(0), period);

  // Analysis results
  extern const int results_size = 100;
  extern double results[results_size] = {0.0};

  // Non-Constant Python analysis initialization switch
  extern bool initialized = false;

  /* Brief :
  *    Sets analysis environment.
  */
  namespace set {

  // Non-Constant Python analysis setup switch
  extern bool state = false;

  /* Brief :
  *    Sets analysis parameters.
  */
  void parameters() {
    // Analysis
    pySet("frequency", frequency);
    pySet("period", period);
  }

  /* Brief :
  *    Sets analysis data.
  */
  void data() {
    // Input & Output
    pySet("data_path", data_path);
    // Sample
    pySet("asset_prices", asset_prices, period);
  }

  /* Brief :
  *    Sets analysis objects.
  */
  void objects() {
    // Session
    pySet("strategy", dll_name);
    pySet("portfolio", asset_list_name);
    pySet("asset", Asset);
    pySet("algorithm", Algo);
  }

  /* Brief :
  *    Sets the environment.
  */
  bool all() {
    // Reset switch
    state = false;
    // Set
    set::parameters();
    set::data();
    set::objects();
    // Finished
    state = true;
    return state;
  }

  } // namespace set

  /* Brief :
  *    Initializes all Python analysis objects.
  */
  bool initialize() {
    // Check switch
    if (!initialized) {
      // Initialize
      set::all();
      pyX("create_strategy()");
      pyX("create_portfolio()");
      for (used_assets) { // All assets and all algos
        pyX("create_asset()");
        pyX("create_algorithm()");
      }
      // All initialized
      initialized = true;
    }
    return initialized;
  }

  /* Brief :
  *    Analyses trade statistics.
  */
  void trades() { pyX("analyze_trades_log()"); }

  /* Brief :
  *    Analyses all strategy data.
  */
  void strategy() {
    // Get price series (outside of 'if' blocks to avoid errors)
    asset_prices = series(price(0), period);
    // Process
    if (bar_counter == Bar && !is(LOOKBACK)) {
      // Update, and analyze environment
      if (set::all()) {
        pyX("analyze_strategy()");
        pyVec("asset_analysis_results", results, results_size);
        // Update counter to the next hours bar
        bar_counter = Bar + frequency;
      }
    }
  }

  /* Brief :
  *    Saves analysis data.
  */
  void save_data() { pyX("save_data()"); }

  } // namespace analysis

} // namespace zdf_python
