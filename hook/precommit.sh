#!/usr/bin/env sh
#
# Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later
#
# Brief :
#   A POSIX compliant Git pre-commit hook script for Python projects.
#
# Quickstart :
#   1. Change execute bit of the file.
#   2. Try the script by manually passing an example module.
#
# Example :
#   chmod u+x ./precommit.sh
#   ./precommit.sh <MODULE>.py
#
# Notes :
#   - Can be used as a standalone script by passing files on the command line.
#   - If any given command fails, script exits with non zero, which stops the commit process.
#   - Word splitting is necessary when passing file names to tools, hence the tool commands with the missing quotes around ${TARGET_FILES} and the 'shellcheck disable=SC2086' lines before them.
#
# Shell options :
#   Default :
#     -e | -o 'errexit' : Exits at non-zero exit status of a command.
#     -u | -o 'nounset' : Unset variables at substitution exits with error.
#   Debugging (call the script with '-d' or '--debug') :
#     -x | -o 'xtrace' : Trace execution by printing commands and their arguments.
set -o 'errexit'  # Exits at non-zero exit status of a command.
set -o 'nounset'  # Unset variables at substitution exits with error.


# Colors
COLOR_RESET="\e[0m"
COLOR_FG_BLACK="\e[1;30m"
COLOR_BG_BLACK="\e[1;40m"
COLOR_FG_RED="\e[1;31m"
COLOR_BG_RED="\e[1;41m"
COLOR_FG_GREEN="\e[1;32m"
COLOR_BG_GREEN="\e[1;42m"
COLOR_FG_YELLOW="\e[1;33m"
COLOR_BG_YELLOW="\e[1;43m"
COLOR_FG_BLUE="\e[1;34m"
COLOR_BG_BLUE="\e[1;44m"
COLOR_FG_MAGENTA="\e[1;35m"
COLOR_BG_MAGENTA="\e[1;45m"
COLOR_FG_CYAN="\e[1;36m"
COLOR_BG_CYAN="\e[1;46m"
COLOR_FG_WHITE="\e[1;37m"
COLOR_BG_WHITE="\e[1;47m"
# Script output colors
COLOR_IO_RESULT="${COLOR_BG_BLUE}${COLOR_FG_BLACK}"
COLOR_IO_NEXT="${COLOR_BG_BLACK}${COLOR_FG_BLUE}"
COLOR_PROCESS_RESULT="${COLOR_BG_MAGENTA}${COLOR_FG_BLACK}"
COLOR_PROCESS_NEXT="${COLOR_BG_BLACK}${COLOR_FG_MAGENTA}"
COLOR_CONDITION_RESULT="${COLOR_BG_CYAN}${COLOR_FG_BLACK}"
COLOR_CONDITION_NEXT="${COLOR_BG_BLACK}${COLOR_FG_CYAN}"
COLOR_INFO_RESULT="${COLOR_BG_WHITE}${COLOR_FG_BLACK}"
COLOR_INFO_NEXT="${COLOR_BG_BLACK}${COLOR_FG_WHITE}"
COLOR_SUCCESS_RESULT="${COLOR_BG_GREEN}${COLOR_FG_BLACK}"
COLOR_SUCCESS_NEXT="${COLOR_BG_BLACK}${COLOR_FG_GREEN}"
COLOR_WARNING_RESULT="${COLOR_BG_YELLOW}${COLOR_FG_BLACK}"
COLOR_WARNING_NEXT="${COLOR_BG_BLACK}${COLOR_FG_YELLOW}"
COLOR_ERROR_RESULT="${COLOR_BG_RED}${COLOR_FG_BLACK}"
COLOR_ERROR_NEXT="${COLOR_BG_BLACK}${COLOR_FG_RED}"


# Help
SCRIPT_HELP=""
SCRIPT_HELP="${COLOR_INFO_NEXT}$(sed --quiet '2,28p' < "$0" | cut --delimiter='#' --fields=2)${COLOR_RESET}"


# Arguments
STAGED_FILES=""
for argument in "$@"; do
    if [ -z "${argument%%-*}" ]; then  # Argument
        case "${argument}" in
            '-d' | '--debug' )
                printf "\n%b Setting debugging options for this shell process...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"
                set -o 'xtrace'  # Trace execution by printing commands and their arguments.
                printf "\n%b Set debugging options for this shell process.%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_RESET}"
                ;;
            '-h' | '--help' )
                printf "\n%b\n" "${SCRIPT_HELP}"
                exit 0
                ;;
            * ) printf "\n%b No configuration found for argument : '%b' %bDisplaying script help...%b\n%b" "${COLOR_IO_RESULT}" "${argument}" "${COLOR_IO_NEXT}" "${COLOR_RESET}" "${SCRIPT_HELP}"
                ;;
        esac
    else  # File
        STAGED_FILES="${STAGED_FILES}${argument} "
    fi
done


# Check this script (when 'shellcheck' is installed and can be used)
if [ -e "$(command -v 'shellcheck')" ]; then
    printf "\n%b Checking %b'%b'...%b\n" "${COLOR_PROCESS_RESULT}" "${COLOR_PROCESS_NEXT}" "$0" "${COLOR_RESET}"
    shellcheck_result=0
    shellcheck -V
    shellcheck "$0"
    shellcheck_result=$?
    printf "\n%b Checked %b'%b'%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_PROCESS_NEXT}" "$0" "${COLOR_RESET}"
    if [ ${shellcheck_result} -ne 0 ]; then
        printf "\n%b ERROR: Shellcheck(%b) found an error.%b Exiting with 1.%b\n" "${COLOR_ERROR_RESULT}" "${shellcheck_result}" "${COLOR_ERROR_NEXT}" "${COLOR_RESET}"
        exit 1  # Error, exit with 1 to abort the commit process
    fi
else
    printf "\n%b INFO: No executable 'shellcheck' found.%b" "${COLOR_INFO_RESULT}" "${COLOR_RESET}"
fi


# Notify script execution start
printf "\n%b Executing...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"


# REPOSITORY #################################################################


# Git
GIT_BIN=""
GIT_BIN="$(command -v 'git')"
GIT_REPOSITORY=false  # Default


printf "\n%b Checking repository...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"
if [ -e "${GIT_BIN}" ]; then
    if "${GIT_BIN}" status 1>/dev/null 2>/dev/null; then
        GIT_REPOSITORY=true
        # Change working directory to repository root
        # shellcheck disable=SC2010
        if ls -a | grep '.git' 1>/dev/null; then
            printf "\n%b Changing working directory to repository root : %b%b" "${COLOR_PROCESS_RESULT}" "${PWD}" "${COLOR_RESET}"
            while ! git ls-files | grep -q 'README'; do
                cd ..
                printf "\n%b Changed working directory to %b%b" "${COLOR_PROCESS_NEXT}" "${PWD}" "${COLOR_RESET}"
            done
            printf "\n%b Changed working directory to repository root : %b%b" "${COLOR_SUCCESS_RESULT}" "${PWD}" "${COLOR_RESET}"
        fi
        # Get staged files
        [ -z "${STAGED_FILES}" ] && \
            STAGED_FILES=$("${GIT_BIN}" diff --staged --name-only)
    else
        printf "\n%b WARNING: Working directory is not inside a git repository : %b %b%b" "${COLOR_WARNING_RESULT}" "${PWD}" "${COLOR_WARNING_NEXT}" "${COLOR_RESET}"
    fi  # [ -n "$("${GIT_BIN}" show 2>/dev/null)" ]
else
    printf "\n%b WARNING: Git '%b' is not executable.%b" "${GIT_BIN}" "${COLOR_WARNING_RESULT}" "${COLOR_RESET}"
fi  # [ -e "${GIT_BIN}" ]
printf "\n%b Checked repository.%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_RESET}"


# Files
EXISTING_FILES=""
PYTHON_SUFFIXES="py"
PYTHON_MODULES=""
UNITTEST_MODULES=""
YAML_SUFFIXES="yml yaml"
YAML_DOCUMENTS=""


printf "\n%b Processing staged files...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"
if [ -z "${STAGED_FILES}" ]; then
    printf "\n%b ERROR: No staged files found.%b Exiting with 1.%b\n" "${COLOR_ERROR_RESULT}" "${COLOR_ERROR_NEXT}" "${COLOR_RESET}"
    exit 1  # Error, exit with 1 to abort the commit process
else
    # Filter
    for file in ${STAGED_FILES}; do
        if [ -f "${file}" ]; then
            EXISTING_FILES="${EXISTING_FILES}${file} "
            # Get Python modules
            for suffix in ${PYTHON_SUFFIXES}; do
                if [ -z "${file%%*."${suffix}"}" ]; then
                    PYTHON_MODULES="${PYTHON_MODULES}${file} "
                    module_name="$(basename "${file%*."${suffix}"}")"
                    UNITTEST_MODULES="${UNITTEST_MODULES}${module_name} "
                fi
            done
            # Get YAML documents
            for suffix in ${YAML_SUFFIXES}; do
                if [ -z "${file%%*."${suffix}"}" ]; then
                    YAML_DOCUMENTS="${YAML_DOCUMENTS}${file} "
                fi
            done
        else
            printf "\n%b WARNING: Not an accessible and/or regular file : '%b'%b" "${COLOR_WARNING_RESULT}" "${file}" "${COLOR_RESET}"
        fi  # [ -f "${file}" ]
    done
fi  # [ -z "${STAGED_FILES}" ]
printf "\n%b Processed staged files.%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_RESET}"


# ENVIRONMENT ################################################################


if [ -z "${PYTHON_MODULES}" ]; then
    printf "\n%b INFO: No staged modules.%b" "${COLOR_INFO_RESULT}" "${COLOR_RESET}"
else
    # PYTHON
    PYTHON_INTERPRETER_BIN="$(command -v 'python3')"
    CONDA_BIN="$(command -v 'conda')"
    ENVIRONMENT_FILES_PATH="./environment/"
    ENVIRONMENT_FILE_NAME="environment"
    # Check dependencies
    if [ -e "${PYTHON_INTERPRETER_BIN}" ] && [ -e "${CONDA_BIN}" ]; then
        # Print version information
        printf "\n%b Comparing environment :%b\n %b\n %b%b" "${COLOR_CONDITION_RESULT}" "${COLOR_CONDITION_NEXT}" "$("${PYTHON_INTERPRETER_BIN}" -VV 2>/dev/null)" "$("${CONDA_BIN}" --version)" "${COLOR_RESET}"
        # Compare current environment with the last committed one
        if ! "${CONDA_BIN}" env export | \
            sed --expression '/^name/d' --expression '/^prefix/d' | \
                diff --color='auto' "${ENVIRONMENT_FILES_PATH}${ENVIRONMENT_FILE_NAME}.yml" -- -; then
            printf "\n%b WARNING: Current environment is different from the last committed one.%b\n" "${COLOR_WARNING_RESULT}" "${COLOR_RESET}"
        fi
        printf "\n%b Comparing environment :%b\n %b\n %b%b" "${COLOR_CONDITION_RESULT}" "${COLOR_CONDITION_NEXT}" "$("${PYTHON_INTERPRETER_BIN}" -VV 2>/dev/null)" "$("${CONDA_BIN}" --version)" "${COLOR_RESET}"
    else
        printf "\n%b ERROR: Python3 interpreter '%b' or Conda '%b' is not executable.%b Exiting with 1.%b\n" "${COLOR_ERROR_RESULT}" "${PYTHON_INTERPRETER_BIN}" "${CONDA_BIN}" "${COLOR_ERROR_NEXT}" "${COLOR_RESET}"
        exit 1  # Error, exit with 1 to abort the commit process
    fi  # [ -e "${PYTHON_INTERPRETER_BIN}" ] && [ -e "${CONDA_BIN}" ]
fi  # [ -z "${PYTHON_MODULES}" ]


# PROCESS ####################################################################


# YAML documents
printf "\n%b Processing YAML documents...%b\n" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"
if [ -z "${YAML_DOCUMENTS}" ]; then
    printf "\n%b INFO: No staged YAML documents.%b" "${COLOR_INFO_RESULT}" "${COLOR_RESET}"
else


    # LINT
    #
    YAMLLINT_BIN="$(command -v 'yamllint')"
    #
    # shellcheck disable=SC2086
    if [ -n "${YAMLLINT_BIN}" ]; then
        "${YAMLLINT_BIN}" --config-data "
                            extends: default

                            ignore: .gitignore

                            rules:
                              document-end:
                                level: warning
                              empty-values: enable
                              float-values: enable
                              line-length:
                                max: 88
                                level: warning
                              truthy: enable
                          " ${YAML_DOCUMENTS}
    fi


fi  # [ -z "${YAML_DOCUMENTS}" ]
printf "\n%b Processed YAML documents.%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_RESET}"


# Python modules
printf "\n%b Processing Python modules...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"
if [ -n "${PYTHON_MODULES}" ]; then  # Avoid passing an empty string to tools


    # SORT IMPORTS
    #
    ISORT_BIN="$(command -v 'isort')"
    #
    # profile: Base profile type to use for configuration.
    # color: Use color in terminal output.
    # gitignore: Treat project as a git repository and ignore files listed in .gitignore.
    # atomic: Ensures the output doesn't save if the resulting file contains syntax errors.
    # combine-star: Ensures that if a star import is present, nothing else is imported from that namespace.
    # force-sort-within-sections: Sort the imports by module, independent of import style.
    # force-alphabetical-sort-within-sections: Force all imports to be sorted alphabetically within a section.
    # only-sections: Causes imports to be sorted based on their sections like STDLIB, THIRDPARTY, etc.
    # only-modified: Suppresses verbose output for non-modified files.
    # overwrite-in-place: Ensures all file flags and modes stay unchanged.
    #
    # shellcheck disable=SC2086
    if [ -n "${ISORT_BIN}" ]; then
        "${ISORT_BIN}" --verbose \
                       --profile='black' \
                       --lines-after-imports 2 \
                       --lines-between-types 2 \
                       --color \
                       --gitignore \
                       --atomic \
                       --combine-star \
                       --force-sort-within-sections \
                       --force-alphabetical-sort-within-sections \
                       --only-sections \
                       --only-modified \
                       --overwrite-in-place \
                       ${PYTHON_MODULES}
    fi


    # ENFORCE CODE STYLE
    #
    BLACK_BIN="$(command -v 'black')"
    #
    # shellcheck disable=SC2086
    if [ -n "${BLACK_BIN}" ]; then
        "${BLACK_BIN}" --verbose \
                       ${PYTHON_MODULES}
    fi


    # LINT
    #
    FLAKE8_BIN="$(command -v 'flake8')"
    #
    # extend-ignore: Selectively add individual patterns without overriding the default comma-separated list of codes to ignore.
    #  F403: ‘from module import *’ used; unable to detect undefined names
    #  F405: 'name' may be undefined, or defined from star imports: 'module'
    #  E203: Whitespace before ':'
    # count: Print the total number of errors.
    # max-line-length: Compatibility with 'black'.
    # show-source: Print the source code generating the error/warning in question.
    # statistics: Count the number of occurrences of each error/warning code and print a report.
    # doctests: Enable PyFlakes syntax checking of doctests in docstrings.
    #
    # shellcheck disable=SC2086
    if [ -n "${FLAKE8_BIN}" ]; then
        "${FLAKE8_BIN}" --verbose \
                        --extend-ignore F403,F405,E203 \
                        --count \
                        --max-line-length 88 \
                        --show-source \
                        --statistics \
                        --doctests \
                        ${PYTHON_MODULES}
    fi


    # TEST
    #
    PYTEST_BIN="$(command -v 'pytest')"
    #
    if [ -n "${PYTEST_BIN}" ]; then
        # PYTEST
        #
        #  doctest-modules: Run doctests in all .py modules.
        #  durations: Show the 'n' slowest setup/test (0: all).
        #  exitfirst: Stop after first failure.
        #  pdb: Drop to Python debugger.
        #  setup-show: Include the setup of fixtures in the output.
        #  showlocals: Show locals in tracebacks.
        #
        # shellcheck disable=SC2086
        "${PYTEST_BIN}" --verbose --verbose \
                        --color='yes' \
                        --doctest-modules \
                        --durations=0 \
                        --exitfirst \
                        --pdb \
                        --setup-show \
                        --showlocals \
                        ${PYTHON_MODULES}


    else
        # UNITTEST (only if the files are importable)
        #
        #  failfast: Exit at the first error.
        #  catch: Catch the first SIGINT, and exit after the current test.
        #  locals: Show locals in tracebacks.
        #
        # shellcheck disable=SC2086
        if [ "$("${PYTHON_INTERPRETER_BIN}" -c "import ${UNITTEST_MODULES% *}" 2>/dev/null)" ]; then
            "${PYTHON_INTERPRETER_BIN}" -m unittest --verbose \
                                                    --catch \
                                                    --failfast \
                                                    --locals \
                                                    ${UNITTEST_MODULES}
        fi


        # DOCTEST
        #
        #  fail-fast: Exit at the first error.
        #  option:
        #   REPORT_NDIFF: Mark differences between lines, and across lines (using difflib.Differ).
        #
        # shellcheck disable=SC2086
        "${PYTHON_INTERPRETER_BIN}" -m doctest --verbose \
                                               --fail-fast \
                                               --option REPORT_NDIFF \
                                               ${PYTHON_MODULES}


    fi  # [ -n "${PYTEST_BIN}" ]


    # REPORT
    #
    COVERAGE_BIN="$(command -v 'coverage')"
    #
    #  branch: Additionally, measure branch coverage.
    #
    # shellcheck disable=SC2086
    if [ -n "${COVERAGE_BIN}" ]; then
        "${COVERAGE_BIN}" run --branch ${PYTHON_MODULES}
        "${COVERAGE_BIN}" html
    fi


    # DOCUMENT
    #
    PYDOC_BIN="$(command -v 'pydoc')"
    #
    #  w: Write HTML documentation.
    #
    # shellcheck disable=SC2086
    if [ -n "${PYDOC_BIN}" ]; then
        "${PYDOC_BIN}" -w ${PYTHON_MODULES}
    fi


    # Update Python module version
    printf "\n%b Updating All Python modules' versions...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"
    README_FILE="./README"
    TAG_VERSION="$(sed --quiet '/Version :/p' < "${README_FILE}" | cut -d' ' -f3)"
    MODULE_VERSION_IDENTIFIER="__version__"
    MODULE_VERSION="${MODULE_VERSION_IDENTIFIER} = '${TAG_VERSION}'"
    # shellcheck disable=SC2086
    sed --in-place "/^${MODULE_VERSION_IDENTIFIER}/c\\${MODULE_VERSION}" ${PYTHON_MODULES}
    printf "\n%b Updated All Python modules' versions.%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_RESET}"


fi  # [ -n "${PYTHON_MODULES}" ]
printf "\n%b Processed Python modules.%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_RESET}"


printf "\n%b Re-staging...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"
# Re-stage all existing, modified and tested files
#
# shellcheck disable=SC2086
${GIT_REPOSITORY} && \
    "${GIT_BIN}" add ${EXISTING_FILES}
printf "\n%b Re-staged.%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_RESET}"


# EXIT #######################################################################


printf "\n%b Executed with no errors. %bExiting with 0.%b\n" "${COLOR_SUCCESS_RESULT}" "${COLOR_SUCCESS_NEXT}" "${COLOR_RESET}"
exit 0  # No errors, exit with 0 for the commit process to continue
