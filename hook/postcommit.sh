#!/usr/bin/env sh
#
# Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later
#
# Brief :
#   A POSIX compliant Git prepare commit message hook script for Python projects.
#
# Quickstart :
#   1. Change execute bit of the file.
#   2. Copy into your projects repository hooks folder (as 'post-commit' with no extension).
#
# Example :
#   chmod u+x ./postcommit.sh
#   cp ./postcommit.sh <PROJECT_REPOSITORY>/.git/hooks/post-commit
#
# Notes :
#   - If any given command fails, script exits with non zero, which stops the commit process.
#
# Shell options :
#   Default :
#     -e | -o 'errexit' : Exits at non-zero exit status of a command.
#     -u | -o 'nounset' : Unset variables at substitution exits with error.
#   Debugging (call the script with '-d' or '--debug') :
#     -x | -o 'xtrace' : Trace execution by printing commands and their arguments.
set -o 'errexit'  # Exits at non-zero exit status of a command.
set -o 'nounset'  # Unset variables at substitution exits with error.


# Colors
COLOR_RESET="\e[0m"
COLOR_FG_BLACK="\e[1;30m"
COLOR_BG_BLACK="\e[1;40m"
COLOR_FG_RED="\e[1;31m"
COLOR_BG_RED="\e[1;41m"
COLOR_FG_GREEN="\e[1;32m"
COLOR_BG_GREEN="\e[1;42m"
COLOR_FG_YELLOW="\e[1;33m"
COLOR_BG_YELLOW="\e[1;43m"
# COLOR_FG_BLUE="\e[1;34m"
# COLOR_BG_BLUE="\e[1;44m"
COLOR_FG_MAGENTA="\e[1;35m"
COLOR_BG_MAGENTA="\e[1;45m"
# COLOR_FG_CYAN="\e[1;36m"
# COLOR_BG_CYAN="\e[1;46m"
# COLOR_FG_WHITE="\e[1;37m"
COLOR_BG_WHITE="\e[1;47m"
# Script output colors
# COLOR_IO_RESULT="${COLOR_BG_BLUE}${COLOR_FG_BLACK}"
# COLOR_IO_NEXT="${COLOR_BG_BLACK}${COLOR_FG_BLUE}"
COLOR_PROCESS_RESULT="${COLOR_BG_MAGENTA}${COLOR_FG_BLACK}"
COLOR_PROCESS_NEXT="${COLOR_BG_BLACK}${COLOR_FG_MAGENTA}"
# COLOR_CONDITION_RESULT="${COLOR_BG_CYAN}${COLOR_FG_BLACK}"
# COLOR_CONDITION_NEXT="${COLOR_BG_BLACK}${COLOR_FG_CYAN}"
COLOR_INFO_RESULT="${COLOR_BG_WHITE}${COLOR_FG_BLACK}"
# COLOR_INFO_NEXT="${COLOR_BG_BLACK}${COLOR_FG_WHITE}"
COLOR_SUCCESS_RESULT="${COLOR_BG_GREEN}${COLOR_FG_BLACK}"
COLOR_SUCCESS_NEXT="${COLOR_BG_BLACK}${COLOR_FG_GREEN}"
COLOR_WARNING_RESULT="${COLOR_BG_YELLOW}${COLOR_FG_BLACK}"
COLOR_WARNING_NEXT="${COLOR_BG_BLACK}${COLOR_FG_YELLOW}"
COLOR_ERROR_RESULT="${COLOR_BG_RED}${COLOR_FG_BLACK}"
COLOR_ERROR_NEXT="${COLOR_BG_BLACK}${COLOR_FG_RED}"


# Check this script (when 'shellcheck' is installed and can be used)
if [ -e "$(command -v 'shellcheck')" ]; then
    printf "\n%b Checking %b'%b'...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_PROCESS_NEXT}" "$0" "${COLOR_RESET}"
    shellcheck_result=0
    shellcheck -V
    shellcheck "$0"
    shellcheck_result=$?
    printf "\n%b Checked %b'%b'%b" "${COLOR_SUCCESS_NEXT}" "${COLOR_PROCESS_NEXT}" "$0" "${COLOR_RESET}"
    if [ ${shellcheck_result} -ne 0 ]; then
        printf "\n%b ERROR: Shellcheck(%b) found an error.%b Exiting with 1.%b" "${COLOR_ERROR_RESULT}" "${shellcheck_result}" "${COLOR_ERROR_NEXT}" "${COLOR_RESET}"
        exit 1  # Error, exit with 1 to abort the commit process
    fi
else
    printf "\n%b INFO: No executable 'shellcheck' found.%b" "${COLOR_INFO_RESULT}" "${COLOR_RESET}"
fi


# Notify script execution start
printf "\n\n%b Executing...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"


# REPOSITORY #################################################################


# Change working directory
printf "\n\n%b Changing working directory '%b' to repository root...%b" "${COLOR_PROCESS_RESULT}" "${PWD}" "${COLOR_RESET}"
while ! git ls-files | grep -q 'README'; do
    cd ..
    printf "\n\n%b Changed working directory to %b%b" "${COLOR_PROCESS_RESULT}" "${PWD}" "${COLOR_RESET}"
done
printf "\n\n%b Changed working directory to repository root '%b'%b" "${COLOR_SUCCESS_RESULT}" "${PWD}" "${COLOR_RESET}"


# Git
GIT_BIN=""
GIT_BIN="$(command -v 'git')"
GIT_REPOSITORY=false  # Default


if [ -e "${GIT_BIN}" ]; then
    if [ -n "$("${GIT_BIN}" show 2>/dev/null)" ]; then
        GIT_REPOSITORY=true
    else
        printf "\n%b WARNING: Working directory is not inside a git repository :%b %b%b" "${COLOR_WARNING_RESULT}" "${PWD}" "${COLOR_WARNING_NEXT}" "${COLOR_RESET}"
    fi
else
    printf "\n%b WARNING: Git '%b' is not executable.%b" "${GIT_BIN}" "${COLOR_WARNING_RESULT}" "${COLOR_RESET}"
fi


# PARAMETERS #################################################################


README_FILE="./README"
CHANGELOG_FILE="./CHANGELOG"


# PROCESS ####################################################################

# Get tag message and version
printf "\n%b Getting tag version and message...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"
TAG_VERSION="$(sed --quiet '/Version :/p' < "${README_FILE}" | cut --delimiter=' ' --fields=3)"
COMMIT_MESSAGE="$(
    sed --quiet '/Version :/,/Brief/p' < "${README_FILE}" | \
        sed --expression '/^Version/d' --expression '/^Brief/d' --expression '/^Quickstart/,//d' | \
            cut --characters '3-'
)"
TAG_MESSAGE="$(echo "${COMMIT_MESSAGE}" | sed --quiet '1p')"
printf "\n%b Got tag version and message :%b %b\n%b%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_SUCCESS_NEXT}" "${TAG_VERSION}" "${TAG_MESSAGE}" "${COLOR_RESET}"


# Update repository
if ${GIT_REPOSITORY}; then
    # Check existing tags -> MOVES THE TAG WITH THE COMMITS
    if [ -n "$("${GIT_BIN}" tag --list "${TAG_VERSION}")" ]; then
        printf "\n%b WARNING: Tag '%b' exists.%b Removing...%b\n" "${COLOR_WARNING_RESULT}" "${TAG_VERSION}" "${COLOR_WARNING_NEXT}" "${COLOR_RESET}"
        "${GIT_BIN}" tag --delete "${TAG_VERSION}"
        printf "\n%b Removed tag :%b %b%b" "${COLOR_WARNING_RESULT}" "${COLOR_WARNING_NEXT}" "${TAG_VERSION}" "${COLOR_RESET}"
    fi
    # Tag commit with project version and commit message header.
    printf "\n%b Tagging last commit with project version...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"
    "${GIT_BIN}" tag --sign "${TAG_VERSION}" --message "${TAG_MESSAGE}"
    printf "\n%b Tagged last commit with project version and message :%b %b\n%b%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_SUCCESS_NEXT}" "${TAG_VERSION}" "${TAG_MESSAGE}" "${COLOR_RESET}"
fi


# Check CHANGELOG file
if [ ! -e "${CHANGELOG_FILE}" ]; then
    printf "\n%b WARNING: CHANGELOG file '%b' does not exist.%b Touching...%b" "${COLOR_WARNING_RESULT}" "${CHANGELOG_FILE}" "${COLOR_WARNING_NEXT}" "${COLOR_RESET}"
    touch "${CHANGELOG_FILE}"
else
    # Check and remove previously recorded changes for this version
    if sed --quiet "/^${TAG_VERSION}/p" < "${CHANGELOG_FILE}" 1>/dev/null; then
        printf "\n%b WARNING: '%b' changes exist in '%b'.%b Removing...%b" "${COLOR_WARNING_RESULT}" "${TAG_VERSION}" "${CHANGELOG_FILE}" "${COLOR_WARNING_NEXT}" "${COLOR_RESET}"
        PREVIOUS_TAG_VERSION="$("${GIT_BIN}" tag --list | sort --reverse | sed --quiet '2p')"
        sed --quiet --in-place "/^${TAG_VERSION}/,/^${PREVIOUS_TAG_VERSION}/d" "${CHANGELOG_FILE}"
        echo "${PREVIOUS_TAG_VERSION}" >> "${CHANGELOG_FILE}"
        printf "\n%b Removed changes for :%b %b%b" "${COLOR_WARNING_RESULT}" "${COLOR_WARNING_NEXT}" "${TAG_VERSION}" "${COLOR_RESET}"
    fi
fi
# Get and append changes to CHANGELOG
COMMIT_MESSAGE="\n${TAG_VERSION}\n${COMMIT_MESSAGE}\n"  # Update commit message
printf "\n%b Appending changes to CHANGELOG file...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"
echo "${COMMIT_MESSAGE}" >> "${CHANGELOG_FILE}"
printf "\n%b Appended changes to CHANGELOG file.%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_RESET}"


# EXIT #######################################################################


printf "\n%b Executed with no errors. %bExiting with 0.%b\n" "${COLOR_SUCCESS_RESULT}" "${COLOR_SUCCESS_NEXT}" "${COLOR_RESET}"
exit 0  # No errors, exit with 0 for the commit process to continue
