#!/usr/bin/env sh
#
# Copyright 2023-2024 Firat Varol <firatv@protonmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later
#
# Brief :
#   A POSIX compliant Git prepare commit message hook script for Python projects.
#
# Quickstart :
#   1. Change execute bit of the file.
#   2. Copy into your projects repository hooks folder (as 'prepare-commit-msg' with no extension).
#
# Example :
#   chmod u+x ./prepare_commit_msg.sh
#   cp ./prepare_commit_msg.sh <PROJECT_REPOSITORY>/.git/hooks/prepare-commit-msg
#
# Notes :
#   - If any given command fails, script exits with non zero, which stops the commit process.
#
# Shell options :
#   Default :
#     -e | -o 'errexit' : Exits at non-zero exit status of a command.
#     -u | -o 'nounset' : Unset variables at substitution exits with error.
set -o 'errexit'  # Exits at non-zero exit status of a command.
set -o 'nounset'  # Unset variables at substitution exits with error.


# Colors
COLOR_RESET="\e[0m"
COLOR_FG_BLACK="\e[1;30m"
COLOR_BG_BLACK="\e[1;40m"
COLOR_FG_RED="\e[1;31m"
COLOR_BG_RED="\e[1;41m"
COLOR_FG_GREEN="\e[1;32m"
COLOR_BG_GREEN="\e[1;42m"
# COLOR_FG_YELLOW="\e[1;33m"
# COLOR_BG_YELLOW="\e[1;43m"
# COLOR_FG_BLUE="\e[1;34m"
# COLOR_BG_BLUE="\e[1;44m"
COLOR_FG_MAGENTA="\e[1;35m"
COLOR_BG_MAGENTA="\e[1;45m"
# COLOR_FG_CYAN="\e[1;36m"
# COLOR_BG_CYAN="\e[1;46m"
# COLOR_FG_WHITE="\e[1;37m"
COLOR_BG_WHITE="\e[1;47m"
# Script output colors
# COLOR_IO_RESULT="${COLOR_BG_BLUE}${COLOR_FG_BLACK}"
# COLOR_IO_NEXT="${COLOR_BG_BLACK}${COLOR_FG_BLUE}"
COLOR_PROCESS_RESULT="${COLOR_BG_MAGENTA}${COLOR_FG_BLACK}"
COLOR_PROCESS_NEXT="${COLOR_BG_BLACK}${COLOR_FG_MAGENTA}"
# COLOR_CONDITION_RESULT="${COLOR_BG_CYAN}${COLOR_FG_BLACK}"
# COLOR_CONDITION_NEXT="${COLOR_BG_BLACK}${COLOR_FG_CYAN}"
COLOR_INFO_RESULT="${COLOR_BG_WHITE}${COLOR_FG_BLACK}"
# COLOR_INFO_NEXT="${COLOR_BG_BLACK}${COLOR_FG_WHITE}"
COLOR_SUCCESS_RESULT="${COLOR_BG_GREEN}${COLOR_FG_BLACK}"
COLOR_SUCCESS_NEXT="${COLOR_BG_BLACK}${COLOR_FG_GREEN}"
# COLOR_WARNING_RESULT="${COLOR_BG_YELLOW}${COLOR_FG_BLACK}"
# COLOR_WARNING_NEXT="${COLOR_BG_BLACK}${COLOR_FG_YELLOW}"
COLOR_ERROR_RESULT="${COLOR_BG_RED}${COLOR_FG_BLACK}"
COLOR_ERROR_NEXT="${COLOR_BG_BLACK}${COLOR_FG_RED}"


# Check this script (when 'shellcheck' is installed and can be used)
if [ -e "$(command -v 'shellcheck')" ]; then
    printf "\n%b Checking %b'%b'...%b\n" "${COLOR_PROCESS_RESULT}" "${COLOR_PROCESS_NEXT}" "$0" "${COLOR_RESET}"
    shellcheck_result=0
    shellcheck -V
    shellcheck "$0"
    shellcheck_result=$?
    printf "\n%b Checked %b'%b'%b" "${COLOR_SUCCESS_NEXT}" "${COLOR_PROCESS_NEXT}" "$0" "${COLOR_RESET}"
    if [ ${shellcheck_result} -ne 0 ]; then
        printf "\n%b ERROR: Shellcheck(%b) found an error.%b Exiting with 1.%b" "${COLOR_ERROR_RESULT}" "${shellcheck_result}" "${COLOR_ERROR_NEXT}" "${COLOR_RESET}"
        exit 1  # Error, exit with 1 to abort the commit process
    fi
else
    printf "\n%b INFO: No executable 'shellcheck' found.%b" "${COLOR_INFO_RESULT}" "${COLOR_RESET}"
fi


# Notify script execution start
printf "\n%b Executing...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"


# COMMIT #####################################################################


# Parameters
COMMIT_MSG_FILE="$1"
# COMMIT_SOURCE="$2"  # Not utilized by this script
# SHA1="$3"  # Not utilized by this script


# REPOSITORY #################################################################


# Change working directory
printf "\n%b Changing working directory '%b' to repository root...%b" "${COLOR_PROCESS_RESULT}" "${PWD}" "${COLOR_RESET}"
while ! git ls-files | grep -q 'README'; do
    cd ..
    printf "\n%b Changed working directory to %b%b" "${COLOR_PROCESS_RESULT}" "${PWD}" "${COLOR_RESET}"
done
printf "\n%b Changed working directory to repository root '%b'%b" "${COLOR_SUCCESS_RESULT}" "${PWD}" "${COLOR_RESET}"


# PROCESS ####################################################################


# Prepare commit message
printf "\n%b Preparing commit message...%b" "${COLOR_PROCESS_RESULT}" "${COLOR_RESET}"
sed --quiet '/Version :/,/Brief/p' < "./README" | \
    sed --expression '/^Version/d' --expression '/^Brief/d' --expression '/^Quickstart/,//d' | \
        cut --characters='3-' > "${COMMIT_MSG_FILE}"
printf "\n%b Prepared commit message :\n%b%b.%b" "${COLOR_SUCCESS_RESULT}" "${COLOR_SUCCESS_NEXT}" "$(cat "${COMMIT_MSG_FILE}")" "${COLOR_RESET}"


# EXIT #######################################################################


printf "\n%b Executed with no errors. %bExiting with 0.%b\n" "${COLOR_SUCCESS_RESULT}" "${COLOR_SUCCESS_NEXT}" "${COLOR_RESET}"
exit 0  # No errors, exit with 0 for the commit process to continue
